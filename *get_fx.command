#!/bin/bash

FILE="$(dirname "$0")"/list_fx.md
DIR="$(dirname "$0")"

echo -e "--- FXs ---\n\n\n" > "$FILE" # -e enables \n interpretation

find "$DIR"/_core/__fx -type f -name "*.orc" -exec basename {} \; | cut -f 1 -d '.' | sort >> "$FILE"