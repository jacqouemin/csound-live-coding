#!/bin/bash

# temp="$(dirname "$0")"/temp.txt
# temp-s="$(dirname "$0")"/temp-s.txt

FILE="$(dirname "$0")"/list_opcode.md
DIR="$(dirname "$0")"/_core/__opcode/*.orc

echo -e "--- OPCODEs ---\n\n\n" > "$FILE" # -e enables \n interpretation
echo -e "opcode | xout | xin | note\r" >> "$FILE"
echo -e "--- | :---: | :---: | ---\r" >> "$FILE"

for f in $DIR 

	do	
		echo "Processing $f file..."
		grep -o '\topcode.*' "$f" |
		perl -p -e 's/\t//' | # remove \t
		perl -p -e 's/\t//' | # remove \t
		perl -p -e 's/ //' | # remove space
		sed 's/,/ | /g' | # sub , with | for table
		sed 's/;/ | /g' | # sub ; with | for table		
		perl -p -e 's/opcode//' >> "$FILE"
	done

# sort "$temp" > "$temp-s"

# | awk ' {print;} NR % 1 == 0 { print ""; }' --- add newline

# cat "$temp-s" >> "$FILE"

# rm "$temp"

# rm "$temp-s"
