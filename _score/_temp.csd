<CsoundSynthesizer>
<CsOptions>
--port=10000 -3 -f
--env:SSDIR+=/Users/j/Documents/my_livecode/_samples
-3 -o Users/j/Desktop/template.wav -W ;;; for file output any platform 
--midioutfile="Users/j/Desktop/template.mid"
</CsOptions>
<CsInstruments>

sr	=	48000	
ksmps	=	12
nchnls	=	2
0dbfs	=	1

giout1	=	1
giout2	=	2

giprintscore	= 1

maxalloc "circle", 8
#include "/Users/j/Documents/my_livecode/_core/00_head.orc"
#include "/Users/j/Documents/my_livecode/_core/00_instruments.orc"
#include "/Users/j/Documents/my_livecode/_core/00_macro.orc"
#include "/Users/j/Documents/my_livecode/_core/00_organs.orc"
#include "/Users/j/Documents/my_livecode/_core/00_tables.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/00_op_before.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/00_op_euclidean.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/00_op_scale.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_chns.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_circles.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_compression.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_drum.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_event.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_externals.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_fx.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_modulator.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_outs.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_ramp.orc"
#include "/Users/j/Documents/my_livecode/_core/__fx/k35.orc"
#include "/Users/j/Documents/my_livecode/_core/__fx/moog.orc"
#include "/Users/j/Documents/my_livecode/_core/__fx/powerranger.orc"
#include "/Users/j/Documents/my_livecode/_core/__fx/shy.orc"
#include "/Users/j/Documents/my_livecode/_core/__fx/sonre.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/_DEFAULT.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/aaron.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/alone.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/axread.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/axvoice.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/bloch.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/cascade.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/cascadexp.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/cascadexpvariation.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/circle.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/distant.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/drum.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/eight.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/elle.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/fairest.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/fim.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/froux.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/haisentitoditom.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/ilookintotheocean.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/infinite.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/juliet.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/mecha.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/puck.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/repuck.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/snug.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/supercluster.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/wendy.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/wingsdown.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/wingsup.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/witches.orc"
#include "/Users/j/Documents/my_livecode/_core/__others/scales.orc"
#include "/Users/j/Documents/my_livecode/_core/__others/sequences.orc"
#include "/Users/j/Documents/my_livecode/_core/__others/zenith.orc"

	instr	route

getmeout("wendy")
getmeout("haisentitoditom")

	endin

</CsInstruments>

<CsScore>
i "route" 0 225
#include "/Users/j/Documents/my_livecode/_score/20Dec30_17_47_13.orc"
                          
</CsScore>             
</CsoundSynthesizer>
