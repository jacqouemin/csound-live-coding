indx	init 0

until indx == ginstrslen do
	isend	= 950 + (nstrnum(gSinstrs[indx])/1000)
	schedule isend, gizero, -1, gSinstrs[indx]
;	prints sprintf("\n%f clear %s\n", isend, gSinstrs[indx])
	indx	+= 1
od

until indx == ginstrslen do
	isend	= 850 + (nstrnum(gSinstrs[indx])/1000)
	schedule isend, gizero, -1, gSinstrs[indx]
;	prints sprintf("\n%f clear %s\n", isend, gSinstrs[indx])
	indx	+= 1
od

	instr 850

Sinstr		= p4

a1		init 0
a2		init 0

S1		sprintf	"%s-1", Sinstr
S2		sprintf	"%s-2", Sinstr

;		ROUTING

a1		chnget S1
a2		chnget S2

		chnmix a1, "mouth-1"
		chnmix a2, "mouth-2"

	endin

	instr 900

Sinstr	= "mouth"

	prints("\n👅 My %s is open and ready to throw up or swallow 👅\n\n", Sinstr)

ilimit	=	.995

a1	chnget sprintf("%s-1", Sinstr)
a2	chnget sprintf("%s-2", Sinstr)

a1	limit a1, -ilimit, ilimit
a2	limit a2, -ilimit, ilimit

a1	*= gkgain
a2	*= gkgain

a1	dcblock2 a1
a2	dcblock2 a2

	outch giout1, a1, giout2, a2, girecout1, a1, girecout2, a2

	chnclear sprintf("%s-1", Sinstr)
	chnclear sprintf("%s-2", Sinstr)

	endin
	alwayson(900)

	instr 950

;	the "clear" instrument

Sinstr	strget	p4
inum	nstrnum Sinstr

	prints("---%s(%i) is digested\n", Sinstr, inum)
	
	chnclear sprintf("%s-1", Sinstr)
	chnclear sprintf("%s-2", Sinstr)

	endin
