puts "Drag an ORC:"
ORC = gets.strip

puts ORC

text = File.read(ORC.to_s)

name = ORC.match(/.*(?=\.)/)

$out = String.new

File.readlines(ORC).each do |line|

  inside_quotes = line.match(/"(.*?)"/).to_s
  quotes        = "\"midioutscore\""

  res      = line.gsub(inside_quotes.to_s, quotes.to_s) 

  $out      << res
  
end

File.open(name.to_s + "_score" + ".orc", "w") { |file| file.write($out) }
