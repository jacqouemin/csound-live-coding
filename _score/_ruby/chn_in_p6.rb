puts "Drag an ORC:"
ORC = gets.strip

puts ORC

text = File.read(ORC.to_s)

name = ORC.match(/.*(?=\.)/)

$out = String.new

instr  = []

File.readlines(ORC).each do |line|

  instr.concat([line.match(/"(.*?)"/).to_s])

end

instr = instr.uniq

File.readlines(ORC).each do |line|

  chn = instr.find_index(line.match(/"(.*?)"/).to_s).to_i + 1
  res = line.match(/(\d+)(?!.*\d)/).to_s + "\t" + chn.to_s

  $out    << line.gsub(/(\d+)(?!.*\d)/, res)

end

File.open(name.to_s + "_score" + ".orc", "w") { |file| file.write($out) }
