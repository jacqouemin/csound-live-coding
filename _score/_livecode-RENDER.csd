<CsoundSynthesizer>
<CsOptions>
--port=10000 -3 -f
--env:SSDIR+=/Users/j/Documents/my_livecode/_samples
</CsOptions>
<CsInstruments>

sr	=	48000	
ksmps	=	24
nchnls	=	2
0dbfs	=	1

giout1	=	1
giout2	=	2

girecout1	= 7
girecout2	= 8

giprintscore	= 1

maxalloc "circle", 8
#include "/Users/j/Documents/my_livecode/_core/00_head.orc"
#include "/Users/j/Documents/my_livecode/_core/00_instruments.orc"
#include "/Users/j/Documents/my_livecode/_core/00_organs.orc"
#include "/Users/j/Documents/my_livecode/_core/00_tab.orc"
#include "/Users/j/Documents/my_livecode/_core/00_tabENVs.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/00_op_before.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/00_op_euclidean.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/00_op_scale.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_chns.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_circles.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_compression.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_drum.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_env.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_event.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_externals.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_modulator.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_outs.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_ramp.orc"
#include "/Users/j/Documents/my_livecode/_core/__fx/comber.orc"
#include "/Users/j/Documents/my_livecode/_core/__fx/flingue.orc"
#include "/Users/j/Documents/my_livecode/_core/__fx/flingue3.orc"
#include "/Users/j/Documents/my_livecode/_core/__fx/k35.orc"
#include "/Users/j/Documents/my_livecode/_core/__fx/moog.orc"
#include "/Users/j/Documents/my_livecode/_core/__fx/powerranger.orc"
#include "/Users/j/Documents/my_livecode/_core/__fx/shy.orc"
#include "/Users/j/Documents/my_livecode/_core/__fx/sonre.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/_DEFAULT.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/aaron.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/alone.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/aloneagain.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/axread.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/axvoice.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/bloch.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/cascade.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/cascadexp.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/cascadexpvariation.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/circle.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/click.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/clicka.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/clickf.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/detuned.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/distant.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/drum.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/eight.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/elle.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/fairest.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/fim.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/froux.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/haisentitoditom.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/ilookintotheocean.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/infinite.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/juliet.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/lualua.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/mecha.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/puck.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/repuck.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/repuckf.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/snug.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/supercluster.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/supersaw.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/wendy.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/wingsdown.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/wingsup.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/witches.orc"
#include "/Users/j/Documents/my_livecode/_core/__others/scales.orc"
#include "/Users/j/Documents/my_livecode/_core/__others/sequences.orc"
#include "/Users/j/Documents/my_livecode/_core/__others/zenith.orc"

	instr route
getmeout("fim")
	endin
	start("route")

</CsInstruments>
<CsScore>
#include "/Users/j/Documents/my_livecode/_score/21Feb19_19_15_31_ZEROTIME.orc"
</CsScore>
</CsoundSynthesizer>
<bsbPanel>
 <label>Widgets</label>
 <objectName/>
 <x>100</x>
 <y>100</y>
 <width>320</width>
 <height>240</height>
 <visible>true</visible>
 <uuid/>
 <bgcolor mode="nobackground">
  <r>255</r>
  <g>255</g>
  <b>255</b>
 </bgcolor>
</bsbPanel>
<bsbPresets>
</bsbPresets>
