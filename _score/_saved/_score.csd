<CsoundSynthesizer>
<CsOptions>
--port=10000 -3 -f
--env:SSDIR+=/Users/j/Documents/my_livecode/_samples
-3 -o Users/j/Desktop/template3.wav -W ;;; for file output any platform 
--midioutfile="Users/j/Desktop/template3.mid"
</CsOptions>
<CsInstruments>

sr	=	48000	
ksmps	=	12
nchnls	=	2
0dbfs	=	1

giout1	=	1
giout2	=	2

giprintscore	= 1

maxalloc "circle", 8
#include "/Users/j/Documents/my_livecode/_core/00_head.orc"
#include "/Users/j/Documents/my_livecode/_core/00_instruments.orc"

gkdiv	init 64 ;max division of main tempo for heart and lungs

;	HEART
;	tempo for heart
gkpulse		init 120 ;tempo for heart in BPM

	instr heart

gkbeatf	= gkpulse / 60			;frequency for a quarter note in Hz
gkbeats	= 1 / (gkpulse / 60)		;time of a quarter note in sec

kph		init 0
kph		phasor (gkpulse / gkdiv) / 60

gkbeatn	init 0				;number of beats from the beginning of session
klast	init -1

if kph < klast then
	gkbeatn += 1
endif

if gkbeatn > 1 then
	turnoff
endif

klast	= kph

	chnset	kph, "heart"

	endin

#include "/Users/j/Documents/my_livecode/_core/00_macro.orc"
#include "/Users/j/Documents/my_livecode/_core/00_tables.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/00_op_before.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/00_op_euclidean.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/00_op_scale.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_chns.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_circles.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_compression.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_drum.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_event.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_externals.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_fx.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_modulator.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_outs.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_ramp.orc"
;#include "/Users/j/Documents/my_livecode/_core/__fx/k35.orc"
;#include "/Users/j/Documents/my_livecode/_core/__fx/moog.orc"
;#include "/Users/j/Documents/my_livecode/_core/__fx/powerranger.orc"
;#include "/Users/j/Documents/my_livecode/_core/__fx/shy.orc"
;#include "/Users/j/Documents/my_livecode/_core/__fx/sonre.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/_DEFAULT.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/aaron.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/alone.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/axread.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/axvoice.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/bloch.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/cascade.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/cascadexp.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/cascadexpvariation.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/circle.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/distant.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/drum.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/eight.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/elle.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/fairest.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/fim.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/froux.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/haisentitoditom.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/ilookintotheocean.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/infinite.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/juliet.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/mecha.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/puck.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/repuck.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/snug.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/supercluster.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/wendy.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/wingsdown.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/wingsup.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/witches.orc"
#include "/Users/j/Documents/my_livecode/_core/__others/scales.orc"
#include "/Users/j/Documents/my_livecode/_core/__others/sequences.orc"
#include "/Users/j/Documents/my_livecode/_core/__others/zenith.orc"

	instr score

kch	= pump(1, fillarray(0, -2, 0, 3))
kchoth	= pump(1, fillarray(0, 0, 1))

if	(eu(7, 12, 16, "heart") == 1) then
	e("aaron",
	gkbeats,
	$ff,
	step("4C", gidorian, pump(8, fillarray(1, 3, 2, 3))),
	step("4C", gidorian, pump(2, fillarray(3, 2, 5, 4, 3, 2, 5, 7)) + kch))
endif

;if	(eu(6, 12, 16, "heart") == 1) then
;	e("wendy",
;	gkbeats,
;	$ff,
;	step("3G", gidorian, pump(8, fillarray(1, 3, 2, 3))),
;	step("3G", gidorian, pump(1, fillarray(3, 2, 5, 4, 3, 2, 5, 7, 3, 2, 5, 7, 3, 2, 5, 7)) + kch))
;endif
;
;if	(eu(3, 12, 32, "heart") == 1) then
;	e("aaron",
;	gkbeats*2,
;	$ff,
;	step("4Bb", giionian, pump(4, fillarray(1, 3, 2, 3, 1, 3, 2, 5))),
;	step("4Bb", giionian, pump(2, fillarray(3, 2, 5, 4, 3, 2, 5, 7, 3, 2, 5, 7, 3, 2, 5, 7)) + kch + kchoth))
;endif

	getmeout("wendy")
	getmeout("cascade")
	getmeout("aaron")

	endin

</CsInstruments>

<CsScore>
i "heart" 4 30
i "score" 4 30
e
</CsScore>
</CsoundSynthesizer>
