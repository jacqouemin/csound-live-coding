<CsoundSynthesizer>
<CsOptions>
--port=10000 -3 -f
--env:SSDIR+=/Users/j/Documents/my_livecode/_samples
-3 -o Users/j/Desktop/template.wav -W ;;; for file output any platform 
--midioutfile="Users/j/Desktop/template.mid"
</CsOptions>
<CsInstruments>

sr	=	48000	
ksmps	=	12
nchnls	=	2
0dbfs	=	1

giout1	=	1
giout2	=	2

giprintscore	= 1

maxalloc "circle", 8
#include "/Users/j/Documents/my_livecode/_core/00_head.orc"
#include "/Users/j/Documents/my_livecode/_core/00_instruments.orc"

gkdiv	init 64 ;max division of main tempo for heart and lungs

;	HEART
;	tempo for heart
gkpulse		init 120 ;tempo for heart in BPM

	instr heart

gkbeatf	= gkpulse / 60			;frequency for a quarter note in Hz
gkbeats	= 1 / (gkpulse / 60)		;time of a quarter note in sec

kph		init 0
kph		phasor (gkpulse / gkdiv) / 60

gkbeatn	init 0				;number of beats from the beginning of session
klast	init -1

if kph < klast then
	gkbeatn += 1
endif

if gkbeatn > 8 then
	turnoff
;	turnoff2 "score"
endif

klast	= kph

	chnset	kph, "heart"
	endin

#include "/Users/j/Documents/my_livecode/_core/00_macro.orc"
#include "/Users/j/Documents/my_livecode/_core/00_tables.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/00_op_before.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/00_op_euclidean.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/00_op_scale.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_chns.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_circles.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_compression.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_drum.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_event.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_externals.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_fx.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_modulator.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_outs.orc"
#include "/Users/j/Documents/my_livecode/_core/__opcode/op_ramp.orc"
#include "/Users/j/Documents/my_livecode/_core/__fx/k35.orc"
#include "/Users/j/Documents/my_livecode/_core/__fx/moog.orc"
#include "/Users/j/Documents/my_livecode/_core/__fx/powerranger.orc"
#include "/Users/j/Documents/my_livecode/_core/__fx/shy.orc"
#include "/Users/j/Documents/my_livecode/_core/__fx/sonre.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/_DEFAULT.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/aaron.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/alone.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/axread.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/axvoice.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/bloch.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/cascade.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/cascadexp.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/cascadexpvariation.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/circle.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/distant.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/drum.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/eight.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/elle.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/fairest.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/fim.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/froux.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/haisentitoditom.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/ilookintotheocean.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/infinite.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/juliet.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/mecha.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/puck.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/repuck.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/snug.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/supercluster.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/wendy.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/wingsdown.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/wingsup.orc"
#include "/Users/j/Documents/my_livecode/_core/__instr/witches.orc"
#include "/Users/j/Documents/my_livecode/_core/__others/scales.orc"
#include "/Users/j/Documents/my_livecode/_core/__others/sequences.orc"
#include "/Users/j/Documents/my_livecode/_core/__others/zenith.orc"

	instr score

gkpulse	= 105 -rall(2, 95)

krel	lfse .25, 16, gkbeatf/16

$if	eu(9, 16, 32, "heart") $then
	e("puck",
	gkbeats*krel,
	$fff,
	step("2Ab", giminor3, pump(23, fillarray(1, 2, 9, 1, 3, 9)), pump(2, (fillarray(-2, 0)))),
	step("3Ab", giminor3, pump(48, fillarray(1, 2, 7, 1, 3, 7)), pump(2, (fillarray(0, -2)))))
endif

routemeout("puck", "shy")
getmeout("puck", .25*comeforme(155))
	endin

</CsInstruments>

<CsScore>
i "heart" 4 155
i "score" 4 155
e
</CsScore>
</CsoundSynthesizer>
