<CsoundSynthesizer>
<CsOptions>
--port=10000 -3 -f
--env:SSDIR+=/Users/j/Documents/my_livecode/_samples
-3 -o Users/j/Desktop/template.wav -W ;;; for file output any platform 
--midioutfile="Users/j/Desktop/template.mid"
</CsOptions>
<CsInstruments>

sr	=	48000	
ksmps	=	12
nchnls	=	2
0dbfs	=	1

	opcode F2M, i, io
iFq, iRnd xin
iNotNum = 12 * (log(iFq/220)/log(2)) + 57
iNotNum = (iRnd == 1 ? round(iNotNum) : iNotNum)
xout iNotNum
	endop

	opcode F2M, k, kO
kFq, kRnd xin
kNotNum = 12 * (log(kFq/220)/log(2)) + 57
kNotNum = (kRnd == 1 ? round(kNotNum) : kNotNum)
xout kNotNum
	endop


	instr midioutscore

idur	= p3
;iamp	= p4
iamp	= 120
icps	= p5

ichn	= p6

inote	F2M icps, 2

	noteondur	ichn, inote, iamp, idur

	endin
                                     
</CsInstruments>

<CsScore>
#include "/Users/j/Documents/my_livecode/_score/20Dec28_17_54_37_ZEROTIME_score_score.orc"  
e       
</CsScore>
</CsoundSynthesizer>
