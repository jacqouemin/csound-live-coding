gkdiv	init 64 ;max division of main tempo for heart and lungs



;	HEART
;	tempo for heart
gkpulse		init 120 ;tempo for heart in BPM

	instr heart

gkbeatf	= gkpulse / 60			;frequency for a quarter note in Hz
gkbeats	= 1 / (gkpulse / 60)		;time of a quarter note in sec

kph		init 0
kph		phasor (gkpulse / gkdiv) / 60

gkbeatn	init 0				;number of beats from the beginning of session
klast	init -1

if (((kph*gkdiv)%1) < klast) then
	gkbeatn += 1
endif

klast	= ((kph*gkdiv)%1)

	chnset	kph, "heart"

	endin
;	schedule("heart", .5, -1)
	alwayson("heart")


;	LUNGS
;	tempo for lungs
gkbreath	init 120

	instr lungs

gkblowf	= gkbreath / 60		;frequency for a quarter note in Hz
gkblows	= 1 / (gkbreath / 60)	;time of a quarter note in sec

kph		init 0
kph		phasor (gkbreath / gkdiv) / 60

gkblown	init 0			;number of beats from the beginning of session
klast	init -1

if (kph < klast) then
	gkblown += 1
endif

klast	= kph

	chnset	kph, "lungs"

	endin
;	schedule("lungs", .5, -1)
	alwayson("lungs")
