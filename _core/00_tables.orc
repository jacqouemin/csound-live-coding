;	GEN
gisine		ftgen	0, 0, 16385, 10, 1					; sine wave
gisquare	ftgen	0, 0, 16385, 7, 1, 8192, 1, 0, -1, 8192, -1		; square wave 
gitri		ftgen	0, 0, 16385, 7, 0, 4096, 1, 8192, -1, 4096, 0		; triangle wave 
gisaw		ftgen	0, 0, 16385, 7, 1, 16384, -1				; sawtooth wave, downward slope

;	CURVEs
girall		ftgen 0, 0, 16385, 5, .00015, 14500, .15, 1500, 1, 385, 1
giacc		ftgen 0, 0, 16385, 5, 1, 1500, .15, 1, 14500, .00015

;	GEN LFOs

gilowsine	 ftgen	0, 0, 8193, 10, 1				; sine wave
gilowsquare	 ftgen	0, 0, 8193, 7, 1, 4096, 1, 0, -1, 4096, -1	; square wave 
gilowtri	 ftgen	0, 0, 8193, 7, 0, 2048, 1, 4096, -1, 2048, 0	; triangle wave 
gilowsaw	 ftgen	0, 0, 8193, 7, 1, 8192, -1			; sawtooth wave, downward slope

gilowasine	 ftgen	0, 0, 8193, -24, gilowsine, 0, 1		; sine wave
gilowasquare	 ftgen	0, 0, 8193, 7, 1, 4096, 1, 0, 0, 4096		; square wave 
gilowatri	 ftgen	0, 0, 8193, 7, 0, 4096, 1, 4096, 0		; triangle wave 
gilowasaw	 ftgen	0, 0, 8193, 7, 1, 8192, 0			; sawtooth wave, downward slope


giben	ftgen 0, 0, 100, -41, 1, 4.6, 2, 5.1, 3, 5.8, 4, 6.7, 5, 7.9, 6, 9.7, 7, 12.5, 8, 17.6, 9, 30.1

