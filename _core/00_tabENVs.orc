;	CLASSIC
;	a 3-points function from linear segments
giclassic_atk		init sr * 5$ms
giclassic_dur		init gienvdur - giclassic_atk
giclassic_int		init 9
giclassic_intdec	init 4
giclassic_dec		init giclassic_intdec / giclassic_int
giclassic_sus		init .15
giclassic_intrel	init giclassic_int-giclassic_intdec
giclassic_rel		init giclassic_intrel / giclassic_int
;-----------------------
print giclassic_atk
print giclassic_dec
print giclassic_sus
print giclassic_rel
;-----------------------
giclassic		ftgen	0, 0, gienvdur, 7, 0, giclassic_atk, 1, giclassic_dur*giclassic_dec, giclassic_sus, giclassic_dur*giclassic_rel, 0
;-----------------------
;
;
;
;	ECLASSIC
;	a 3-points function from segments of exponential curves
gieclassic_atk		init sr * 5$ms
gieclassic_dur		init gienvdur - gieclassic_atk
gieclassic_int		init 9
gieclassic_intdec	init 5
gieclassic_dec		init gieclassic_intdec / gieclassic_int
gieclassic_sus		init .15
gieclassic_intrel	init gieclassic_int-gieclassic_intdec
gieclassic_rel		init gieclassic_intrel / gieclassic_int
;-----------------------
print gieclassic_atk
print gieclassic_dec
print gieclassic_sus
print gieclassic_rel
;-----------------------
gieclassic		ftgen	0, 0, gienvdur, 5, giexpzero, gieclassic_atk, 1, gieclassic_dur*gieclassic_dec, gieclassic_sus, gieclassic_dur*gieclassic_rel, giexpzero
;-----------------------
;
;
;
;	CLASSICVAR
;	a 3-points function from linear segments
giclassicvar_int	init 35
giclassicvar_intatk	init 1
giclassicvar_intdec	init 5
giclassicvar_intrel	init giclassicvar_int-giclassicvar_intatk-giclassicvar_intdec
;-----------------------
giclassicvar_atk	init giclassicvar_intatk / giclassicvar_int
giclassicvar_dec	init giclassicvar_intdec / giclassicvar_int
giclassicvar_sus	init .5
giclassicvar_rel	init giclassicvar_intrel / giclassicvar_int
;-----------------------
print giclassicvar_atk; 1/35
print giclassicvar_dec; 5/35
print giclassicvar_sus; .5
print giclassicvar_rel; 29/35
;-----------------------
giclassicvar		ftgen	0, 0, gienvdur, 7, 0, gienvdur*giclassicvar_atk, 1, gienvdur*giclassicvar_dec, giclassicvar_sus, gienvdur*giclassicvar_rel, 0
;-----------------------
;
;
;
;	ECLASSICVAR
;	a 3-points function from segments of exponential curves
gieclassicvar_int	init 35
gieclassicvar_intatk	init 1
gieclassicvar_intdec	init 5
gieclassicvar_intrel	init gieclassicvar_int-gieclassicvar_intatk-gieclassicvar_intdec
;-----------------------
gieclassicvar_atk	init gieclassicvar_intatk / gieclassicvar_int
gieclassicvar_dec	init gieclassicvar_intdec / gieclassicvar_int
gieclassicvar_sus	init .5
gieclassicvar_rel	init gieclassicvar_intrel / gieclassicvar_int
;-----------------------
print gieclassicvar_atk; 1/35
print gieclassicvar_dec; 5/35
print gieclassicvar_sus; .5
print gieclassicvar_rel; 29/35
;-----------------------
gieclassicvar		ftgen	0, 0, gienvdur, 5, giexpzero, gienvdur*gieclassicvar_atk, 1, gienvdur*gieclassicvar_dec, gieclassicvar_sus, gienvdur*gieclassicvar_rel, giexpzero
;-----------------------
;
;
;
;	KAZAN
;	a 3-points function from segments of exponential curves
gikazan_int		init 11
gikazan_intatk		init 1
gikazan_intdec		init 9
gikazan_intrel		init gikazan_int-gikazan_intatk-gikazan_intdec
;-----------------------
gikazan_atk		init gikazan_intatk / gikazan_int
gikazan_dec		init gikazan_intdec / gikazan_int
gikazan_sus		init .35
gikazan_rel		init gikazan_intrel / gikazan_int
;-----------------------
print gikazan_atk
print gikazan_dec
print gikazan_sus
print gikazan_rel
;-----------------------
gikazan			ftgen	0, 0, gienvdur, 5, giexpzero, gienvdur*gikazan_atk, 1, gienvdur*gikazan_dec, gikazan_sus, gienvdur*gikazan_rel, giexpzero
;-----------------------
;
;
;
gioneboob	ftgen	0, 0, gienvdur, 8, 0, gienvdur*.25, 1, gienvdur*.5, 1, gienvdur*.25, 0
;
;
;
;	LIKEAREV
;	a 6-points function from linear segments
gilikearev_atk		init sr * 95$ms
gilikearev_dur		init gienvdur - gilikearev_atk
gilikearev_int		init 32
gilikearev_intdec		init 1
gilikearev_dec		init gilikearev_intdec / gilikearev_int

gilikearev_sus1		init .15
gilikearev_intrel1		init 3
gilikearev_rel1		init gilikearev_intrel1 / gilikearev_int

gilikearev_sus2		init .05
gilikearev_intrel2		init gilikearev_int-gilikearev_intdec-gilikearev_intrel1
gilikearev_rel2		init gilikearev_intrel2 / gilikearev_int

;-----------------------
print gilikearev_atk
print gilikearev_dec
print gilikearev_sus1
print gilikearev_rel1
print gilikearev_sus2
print gilikearev_rel2
;-----------------------
gilikearev		ftgen	0, 0, gienvdur, 7, 0, gilikearev_atk, 1, gilikearev_dur*gilikearev_dec, gilikearev_sus1, gilikearev_dur*gilikearev_rel1, gilikearev_sus2, gilikearev_dur*gilikearev_rel2, 0
;-----------------------
