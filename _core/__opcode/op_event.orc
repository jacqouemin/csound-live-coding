	opcode	eran, 0, kSkOOOOOO
	iceil, Sinstr, ip3, ip4, ip5, ip6, ip7, ip8, ip9 xin

	iran	random	0, iceil

	event("i", Sinstr, iran, ip3, ip4, ip5, ip6, ip7, ip8, ip9)
	endop

	opcode	edrum, 0, SkkP
Sfile, kpitch, kamp, kdur xin
schedulek("drum", 0, .05, Sfile, kpitch, kamp, kdur)
		endop

	instr gotomidi

idur	= p3
iamp	= p4 * 127
icps	= p5

Sinstr	strget p6

ichn	= 1

indx	init 0

while 	indx < ginstrslen do

	ipos	strcmp Sinstr, gSinstrs[indx]

	if	ipos == 0 then
		itrack = indx
	endif

	indx += 1

od

inote	F2M icps, 2

	noteondur2	ichn, inote, iamp, idur

	endin

	opcode	e, 0, SkkkkOOOO
	Sinstr, kdur, kamp, kenv, kcps1, kcps2, kcps3, kcps4, kcps5 xin

if	kdur > 50$ms && kamp > 0 then

		kdur	limit	kdur, gizero, 120	;limit kdur from gizero to 120
	/*
		if	(kcps1 != 0 && kcps2 == 0 && kcps3 == 0 && kcps4 == 0 && kcps5 == 0) then
			event("i", Sinstr, gkzero, kdur, kamp, kcps1)
			midiout 144, 1, F2M(kcps1), (kamp*127)

		elseif	(kcps1 != 0 && kcps2 != 0 && kcps3 == 0 && kcps4 == 0 && kcps5 == 0) then
			kamp /= 2
			event("i", Sinstr, gkzero, kdur, kamp, kcps1)
			event("i", Sinstr, gkzero, kdur, kamp, kcps2)
			midiout 144, 1, F2M(kcps1), (kamp*127)
			midiout 144, 2, F2M(kcps2), (kamp*127)

		elseif	(kcps1 != 0 && kcps2 != 0 && kcps3 != 0 && kcps4 == 0 && kcps5 == 0) then
			kamp /= 3
			event("i", Sinstr, gkzero, kdur, kamp, kcps1)
			event("i", Sinstr, gkzero, kdur, kamp, kcps2)
			event("i", Sinstr, gkzero, kdur, kamp, kcps3)
			midiout 144, 1, F2M(kcps1), (kamp*127)
			midiout 144, 2, F2M(kcps2), (kamp*127)
			midiout 144, 3, F2M(kcps3), (kamp*127)

		elseif	(kcps1 != 0 && kcps2 != 0 && kcps3 != 0 && kcps4 != 0 && kcps5 == 0) then
			kamp /= 4
			event("i", Sinstr, gkzero, kdur, kamp, kcps1)
			event("i", Sinstr, gkzero, kdur, kamp, kcps2)
			event("i", Sinstr, gkzero, kdur, kamp, kcps3)
			event("i", Sinstr, gkzero, kdur, kamp, kcps4)
			midiout 144, 1, F2M(kcps1), (kamp*127)
			midiout 144, 2, F2M(kcps2), (kamp*127)
			midiout 144, 3, F2M(kcps3), (kamp*127)
			midiout 144, 4, F2M(kcps4), (kamp*127)

		elseif	(kcps1 != 0 && kcps2 != 0 && kcps3 != 0 && kcps4 != 0 && kcps5 != 0) then
			kamp /= 5
			event("i", Sinstr, gkzero, kdur, kamp, kcps1)
			event("i", Sinstr, gkzero, kdur, kamp, kcps2)
			event("i", Sinstr, gkzero, kdur, kamp, kcps3)
			event("i", Sinstr, gkzero, kdur, kamp, kcps4)
			event("i", Sinstr, gkzero, kdur, kamp, kcps5)
			midiout 144, 1, F2M(kcps1), (kamp*127)
			midiout 144, 2, F2M(kcps2), (kamp*127)
			midiout 144, 3, F2M(kcps3), (kamp*127)
			midiout 144, 4, F2M(kcps4), (kamp*127)
			midiout 144, 5, F2M(kcps5), (kamp*127)

		endif
	*/

		;	amp depends on how many notes
		if	(kcps2 != 0 && kcps3 == 0 && kcps4 == 0 && kcps5 == 0) then
		kamp /= 2
		elseif	(kcps2 != 0 && kcps3 != 0 && kcps4 == 0 && kcps5 == 0) then
		kamp /= 3
		elseif	(kcps2 != 0 && kcps3 != 0 && kcps4 != 0 && kcps5 == 0) then
		kamp /= 4
		elseif	(kcps2 != 0 && kcps3 != 0 && kcps4 != 0 && kcps5 != 0) then
		kamp /= 5
		endif

		;	generate event
		if	(kcps1 != 0) then
			schedulek	Sinstr, gkzero, kdur, kamp, kenv, kcps1

			if	(giprintscore == 1) then
			kdone	system gkabstime, sprintfk("echo \'i\t\"%s\"\t%f\t%f\t%f\t%i\t%f\' >> %s", Sinstr, gkabstime, kdur, kamp, kenv, kcps1, gScorename)
			endif

			schedulek "gotomidi", gkzero, kdur, kamp, kcps1, Sinstr
			;midiout 144, 1, F2M(kcps1), int(kamp*127)
		endif

		if	(kcps2 != 0) then
			schedulek	Sinstr, gkzero, kdur, kamp, kenv, kcps2

			if	(giprintscore == 1) then
			kdone	system gkabstime, sprintfk("echo \'i\t\"%s\"\t%f\t%f\t%f\t%i\t%f\' >> %s", Sinstr, gkabstime, kdur, kamp, kenv, kcps2, gScorename)
			endif

			schedulek "gotomidi", gkzero, kdur, kamp, kcps2, Sinstr
			;midiout 144, 2, F2M(kcps2), int(kamp*127)
		endif

		if	(kcps3 != 0) then
			schedulek	Sinstr, gkzero, kdur, kamp, kenv, kcps3

			if	(giprintscore == 1) then
			kdone	system gkabstime, sprintfk("echo \'i\t\"%s\"\t%f\t%f\t%f\t%i\t%f\' >> %s", Sinstr, gkabstime, kdur, kamp, kenv, kcps3, gScorename)
			endif

			schedulek "gotomidi", gkzero, kdur, kamp, kcps3, Sinstr
			;midiout 144, 3, F2M(kcps3), int(kamp*127)
		endif

		if	(kcps4 != 0) then
			schedulek	Sinstr, gkzero, kdur, kamp, kenv, kcps4

			if	(giprintscore == 1) then
			kdone	system gkabstime, sprintfk("echo \'i\t\"%s\"\t%f\t%f\t%f\t%i\t%f\' >> %s", Sinstr, gkabstime, kdur, kamp, kenv, kcps4, gScorename)
			endif

			schedulek "gotomidi", gkzero, kdur, kamp, kcps4, Sinstr
			;midiout 144, 4, F2M(kcps4), int(kamp*127)
		endif

		if	(kcps5 != 0) then
			schedulek	Sinstr, gkzero, kdur, kamp, kenv, kcps5

			if	(giprintscore == 1) then
			kdone	system gkabstime, sprintfk("echo \'i\t\"%s\"\t%f\t%f\t%f\t%i\t%f\' >> %s", Sinstr, gkabstime, kdur, kamp, kenv, kcps5, gScorename)
			endif

			schedulek "gotomidi", gkzero, kdur, kamp, kcps5, Sinstr
			;midiout 144, 5, F2M(kcps5), int(kamp*127)
		endif
endif

		endop

	opcode	d, 0, SkkkkOOOO
	Sinstr, kdur, kamp, kenv, kcps1, kcps2, kcps3, kcps4, kcps5 xin

if	kdur > 50$ms && kamp > 0 then

		kdur	limit	kdur, gizero, 120	;limit kdur from gizero to 120
	/*
		if	(kcps1 != 0 && kcps2 == 0 && kcps3 == 0 && kcps4 == 0 && kcps5 == 0) then
			event("i", Sinstr, gkzero, kdur, kamp, kcps1)
			midiout 144, 1, F2M(kcps1), (kamp*127)

		elseif	(kcps1 != 0 && kcps2 != 0 && kcps3 == 0 && kcps4 == 0 && kcps5 == 0) then
			kamp /= 2
			event("i", Sinstr, gkzero, kdur, kamp, kcps1)
			event("i", Sinstr, gkzero, kdur, kamp, kcps2)
			midiout 144, 1, F2M(kcps1), (kamp*127)
			midiout 144, 2, F2M(kcps2), (kamp*127)

		elseif	(kcps1 != 0 && kcps2 != 0 && kcps3 != 0 && kcps4 == 0 && kcps5 == 0) then
			kamp /= 3
			event("i", Sinstr, gkzero, kdur, kamp, kcps1)
			event("i", Sinstr, gkzero, kdur, kamp, kcps2)
			event("i", Sinstr, gkzero, kdur, kamp, kcps3)
			midiout 144, 1, F2M(kcps1), (kamp*127)
			midiout 144, 2, F2M(kcps2), (kamp*127)
			midiout 144, 3, F2M(kcps3), (kamp*127)

		elseif	(kcps1 != 0 && kcps2 != 0 && kcps3 != 0 && kcps4 != 0 && kcps5 == 0) then
			kamp /= 4
			event("i", Sinstr, gkzero, kdur, kamp, kcps1)
			event("i", Sinstr, gkzero, kdur, kamp, kcps2)
			event("i", Sinstr, gkzero, kdur, kamp, kcps3)
			event("i", Sinstr, gkzero, kdur, kamp, kcps4)
			midiout 144, 1, F2M(kcps1), (kamp*127)
			midiout 144, 2, F2M(kcps2), (kamp*127)
			midiout 144, 3, F2M(kcps3), (kamp*127)
			midiout 144, 4, F2M(kcps4), (kamp*127)

		elseif	(kcps1 != 0 && kcps2 != 0 && kcps3 != 0 && kcps4 != 0 && kcps5 != 0) then
			kamp /= 5
			event("i", Sinstr, gkzero, kdur, kamp, kcps1)
			event("i", Sinstr, gkzero, kdur, kamp, kcps2)
			event("i", Sinstr, gkzero, kdur, kamp, kcps3)
			event("i", Sinstr, gkzero, kdur, kamp, kcps4)
			event("i", Sinstr, gkzero, kdur, kamp, kcps5)
			midiout 144, 1, F2M(kcps1), (kamp*127)
			midiout 144, 2, F2M(kcps2), (kamp*127)
			midiout 144, 3, F2M(kcps3), (kamp*127)
			midiout 144, 4, F2M(kcps4), (kamp*127)
			midiout 144, 5, F2M(kcps5), (kamp*127)

		endif
	*/

		;	amp depends on how many notes
		if	(kcps2 != 0 && kcps3 == 0 && kcps4 == 0 && kcps5 == 0) then
		kamp /= 2
		elseif	(kcps2 != 0 && kcps3 != 0 && kcps4 == 0 && kcps5 == 0) then
		kamp /= 3
		elseif	(kcps2 != 0 && kcps3 != 0 && kcps4 != 0 && kcps5 == 0) then
		kamp /= 4
		elseif	(kcps2 != 0 && kcps3 != 0 && kcps4 != 0 && kcps5 != 0) then
		kamp /= 5
		endif

		;	generate event
		if	(kcps1 != 0) then
			schedulek	Sinstr, gkzero, kdur, kamp, -kenv, kcps1
			schedulek	Sinstr, kdur, kdur, kamp, kenv, kcps1

			if	(giprintscore == 1) then
			kdone	system gkabstime, sprintfk("echo \'i\t\"%s\"\t%f\t%f\t%f\t%i\t%f\' >> %s", Sinstr, gkabstime, kdur, kamp, kenv, kcps1, gScorename)
			endif

			schedulek "gotomidi", gkzero, kdur, kamp, kcps1, Sinstr
			;midiout 144, 1, F2M(kcps1), int(kamp*127)
		endif

		if	(kcps2 != 0) then
			schedulek	Sinstr, gkzero, kdur, kamp, -kenv, kcps2
			schedulek	Sinstr, kdur, kdur, kamp, kenv, kcps2

			if	(giprintscore == 1) then
			kdone	system gkabstime, sprintfk("echo \'i\t\"%s\"\t%f\t%f\t%f\t%i\t%f\' >> %s", Sinstr, gkabstime, kdur, kamp, kenv, kcps2, gScorename)
			endif

			schedulek "gotomidi", gkzero, kdur, kamp, kcps2, Sinstr
			;midiout 144, 2, F2M(kcps2), int(kamp*127)
		endif

		if	(kcps3 != 0) then
			schedulek	Sinstr, gkzero, kdur, kamp, -kenv, kcps3
			schedulek	Sinstr, kdur, kdur, kamp, kenv, kcps3

			if	(giprintscore == 1) then
			kdone	system gkabstime, sprintfk("echo \'i\t\"%s\"\t%f\t%f\t%f\t%i\t%f\' >> %s", Sinstr, gkabstime, kdur, kamp, kenv, kcps3, gScorename)
			endif

			schedulek "gotomidi", gkzero, kdur, kamp, kcps3, Sinstr
			;midiout 144, 3, F2M(kcps3), int(kamp*127)
		endif

		if	(kcps4 != 0) then
			schedulek	Sinstr, gkzero, kdur, kamp, -kenv, kcps4
			schedulek	Sinstr, kdur, kdur, kamp, kenv, kcps4

			if	(giprintscore == 1) then
			kdone	system gkabstime, sprintfk("echo \'i\t\"%s\"\t%f\t%f\t%f\t%i\t%f\' >> %s", Sinstr, gkabstime, kdur, kamp, kenv, kcps4, gScorename)
			endif

			schedulek "gotomidi", gkzero, kdur, kamp, kcps4, Sinstr
			;midiout 144, 4, F2M(kcps4), int(kamp*127)
		endif

		if	(kcps5 != 0) then
			schedulek	Sinstr, gkzero, kdur, kamp, -kenv, kcps5
			schedulek	Sinstr, kdur, kdur, kamp, kenv, kcps5

			if	(giprintscore == 1) then
			kdone	system gkabstime, sprintfk("echo \'i\t\"%s\"\t%f\t%f\t%f\t%i\t%f\' >> %s", Sinstr, gkabstime, kdur, kamp, kenv, kcps5, gScorename)
			endif

			schedulek "gotomidi", gkzero, kdur, kamp, kcps5, Sinstr
			;midiout 144, 5, F2M(kcps5), int(kamp*127)
		endif
endif

		endop


	opcode	e, 0, Si
	Sinstr, idur xin

	schedule Sinstr, 0, idur

	endop
 
	opcode	hmelody, 0, SkkkSk[]
	Sinstr, kdiv, kdur, kamp, Sroot, kmelody[] xin


kcount	init 0
kcycle	init 0

korgan	chnget	"heart"
kph	=	(korgan * kdiv) % 1

klast	init -1
if (kph < klast) then
	kcycle = 1
endif
klast	= kph

klen		= lenarray(kmelody) / 4
klens[]		genarray 0, klen - 1
kndx		= klens[kcount] * 4
if (kph > kmelody[kndx] && kcycle == 1) then

	event("i", Sinstr, gkzero, gkbeats * (gkdiv/kdiv) * kmelody[kndx+2] * kdur, kamp * kmelody[kndx+3], chromatic(Sroot, kmelody[kndx+1]))

kdone	system gkabstime, sprintfk("echo \'i\t\"%s\"\t%f\t%f\t%f\t%f\' >> %s", Sinstr, gkabstime + gkzero, gkbeats * (gkdiv/kdiv) * kmelody[kndx+2] * kdur, kamp * kmelody[kndx+3], chromatic(Sroot, kmelody[kndx+1]), gScorename)

	midiout 144, 1, F2M(chromatic(Sroot, kmelody[kndx+1])), (kamp*127)

	kcount += 1

	if (kph > kmelody[(klen - 1) * 4]) then
		kcycle = 0
	endif

endif

if (kph < kmelody[(klen - 1) * 4]) then
	kcycle = 1
endif

if (kcount == klen) then
	kcount = 0
endif

		endop

opcode mtofk,k,k
        kmid xin
        kout = 440 * exp(0.0577622645 * (kmid - 69))
        xout kout
endop

ierr	lua_dofile "./__others/itv.lua"

	opcode intv, k, k 

	ktrig xin

iArr[] fillarray 4, 2.25, 75, 50, 90
kArr[] init 3
kArr[0] = ktrig
kArr[1] = 1	
kArr[2] = 1

kpch	= lua_obj("itv", iArr, kArr)
kfq	= mtofk(kpch)

	xout kfq
	endop


