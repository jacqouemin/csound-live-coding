	opcode	flingue, 0, SaOP
Sin, adel, kfb, kgain xin

kfb	init .5

adel	limit adel, gizero, 35

Sin1	sprintf	"%s-1", Sin
Sin2	sprintf	"%s-2", Sin

ain1	chnget Sin1
ain2	chnget Sin2

;	instrument
a1	flanger ain1, adel, kfb
a2	flanger ain2, adel+(adel/5), kfb

a1	*= a(kgain)
a2	*= a(kgain)

	chnmix a1, "mouth-1"
	chnmix a2, "mouth-2"

		endop
