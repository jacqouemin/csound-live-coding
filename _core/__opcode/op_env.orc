gimorf		ftgen 0, 0, gienvdur, 10, 1

	opcode morpheus, i, kiiooo
	kndx, ift1, ift2, ift3, ift4, ift5 xin

ifno		init giclassic

iftmorf1	abs	floor(ift1)
iftmorf2	abs	floor(ift2)
iftmorf3	abs	floor(ift3)
iftmorf4	abs	floor(ift4)
iftmorf5	abs	floor(ift5)

ifact		init .995

if	ift3==0 then
	ifno	ftgenonce 0, 0, 2, -2, iftmorf1, iftmorf2
		ftmorf kndx*ifact, ifno, gimorf
elseif	ift3>0&&ift4==0 then
	ifno	ftgenonce 0, 0, 3, -2, iftmorf1, iftmorf2, iftmorf3
		ftmorf kndx*(ifact+1), ifno, gimorf
elseif	ift4>0&&ift5==0 then
	ifno	ftgenonce 0, 0, 4, -2, iftmorf1, iftmorf2, iftmorf3, iftmorf4
		ftmorf kndx*(ifact+2), ifno, gimorf
elseif	ift5>0 then
	ifno	ftgenonce 0, 0, 5, -2, iftmorf1, iftmorf2, iftmorf3, iftmorf4, iftmorf5
		ftmorf kndx*(ifact+3), ifno, gimorf
endif

	xout gimorf
	endop



	opcode envgen, a, ii
	idur, iftenv	xin
	
ifenvmod	init	floor(iftenv)-iftenv
iftenvreal	abs	floor(iftenv)

if	ifenvmod == 0 then

	if	iftenv > 0 then
		alinenv	linseg 0, idur, gienvdur
	else
		alinenv	linseg gienvdur, idur, 0
	endif

else 

	iatk	abs ifenvmod

	if	iftenv > 0 then

		ires	init 0
		indx	init -1
		ilast	init idur-iatk
		
		until ires==1 do
			indx	+= 1
			ires	tablei indx, abs(iftenv)
		od

		if	iatk<ilast then
			alinenv	linseg 0, iatk, indx, ilast, gienvdur
		else
			alinenv	linseg 0, idur, gienvdur
		endif

	else

		ires	init 0
		indx	init -1
		ilast	init idur-iatk
			
		until ires==1 do
			indx	+= 1
			ires	table indx, abs(iftenv)
		od
		
		if	iatk<ilast then
			alinenv	linseg gienvdur, ilast, indx, iatk, 0
		else
			alinenv	linseg gienvdur, idur, 0
		endif

	endif

endif

if	iftenvreal==gimorf then
	amorf	tablei alinenv, iftenvreal
	aramp	tablei alinenv, gisotrap
	aenv	= amorf*aramp
else
	aenv	tablei alinenv, iftenvreal
endif

	xout aenv
	endop
