gkclearchns[]	init 128

	opcode	routemeout, 0, SSP
Sin, Sout, kgain xin

a1	init 0
a2	init 0

a1	chnget sprintf("%s-1", Sin)
a2	chnget sprintf("%s-2", Sin)

if (kgain == 1) then
	kgain = $fff
endif

a1	*= a(kgain)
a2	*= a(kgain)

	chnmix a1, sprintf("%s-1", Sout)
	chnmix a2, sprintf("%s-2", Sout)

		endop

	opcode	routemeout, 0, SiiP
Sin, iout1, iout2, kgain xin

a1	chnget sprintf("%s-1", Sin)
a2	chnget sprintf("%s-2", Sin)

if (kgain == 1) then
	kgain = $fff
endif

a1	*= a(kgain)
a2	*= a(kgain)

ilimit	=	.995

a1	limit a1, -ilimit, ilimit
a2	limit a2, -ilimit, ilimit

a1	dcblock2 a1
a2	dcblock2 a2

		outch iout1, a1, iout2, a2

;		CLEAR
		chnclear sprintf("%s-1", Sin)
		chnclear sprintf("%s-2", Sin)

		endop


	opcode	getmeout, 0, SP

Sinstr, kgain	xin

a1		init 0
a2		init 0

S1		sprintf	"%s-1", Sinstr
S2		sprintf	"%s-2", Sinstr

;		ROUTING

a1		chnget S1
a2		chnget S2

a1		*= portk(kgain, 5$ms)
a2		*= portk(kgain, 5$ms)

		chnmix a1, "mouth-1"
		chnmix a2, "mouth-2"

		endop

	opcode	xmeout, 0, SSi
Sin, Sout, it	xin

Sin1	sprintf	"%s-1", Sin
Sin2	sprintf	"%s-2", Sin

Sout1	sprintf	"%s-1", Sout
Sout2	sprintf	"%s-2", Sout

;	routing
ain1	chnget Sin1
ain2	chnget Sin2

aout1	chnget Sout1
aout2	chnget Sout2

;	instrument

ain1	*= linseg:a(1, it, 0)
ain2	*= linseg:a(1, it, 0)

aout1	*= linseg:a(0, it, 1)
aout2	*= linseg:a(0, it, 1)

a1	= ain1 + aout1
a2	= ain2 + aout2

	chnmix a1, "mouth-1"
	chnmix a2, "mouth-2"

		endop
