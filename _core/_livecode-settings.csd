<CsoundSynthesizer>
<CsOptions>
--port=10000 -3 -f
--env:SSDIR=/Users/j/Documents/my_livecode/_samples
--env:OPCODE6DIR64=/Library/Frameworks/CsoundLib64.framework/Versions/6.0/Resources/Opcodes64
</CsOptions>
<CsInstruments>

sr	=	48000	
ksmps	=	32
nchnls	=	2
0dbfs	=	1

giout1	=	1
giout2	=	2
girecout1	= 3
girecout2	= 4

giprintscore	= 1
