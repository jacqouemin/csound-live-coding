<CsoundSynthesizer>
<CsOptions>
--port=10000 -3 -f
--env:SSDIR+=/Users/j/Documents/my_livecode/_samples
</CsOptions>
<CsInstruments>

sr	=	48000	
ksmps	=	16
nchnls	=	2
0dbfs	=	1

#include "/Users/j/Documents/my_livecode/_core/_livecode-settings.orc"

gienvdur		init 8192

;	EAU
;	GEN08 — Generate a piecewise cubic spline curve.
;	y + x

gieau_gen		init 8
	
gieau_y1		init 0
gieau_x1		init gienvdur/4

gieau_y2		init .125
gieau_x2		init gieau_x1+gienvdur/4

gieau_y3		init 1
gieau_x3		init gieau_x2+gienvdur/4

gieau_y4		init 0
gieau_x4		init gieau_x3+gienvdur/4


;-----------------------
gieau		ftgen	0, 0, gienvdur, gieau_gen,
			gieau_y1, gieau_x1
			gieau_y2, gieau_x2
			gieau_y3, gieau_x3
			gieau_y4, gieau_x4
;-----------------------

	instr 1
print gieau
	endin

</CsInstruments>
<CsScore>
i 1 0 1
</CsScore>
</CsoundSynthesizer>
<bsbPanel>
 <label>Widgets</label>
 <objectName/>
 <x>100</x>
 <y>100</y>
 <width>320</width>
 <height>240</height>
 <visible>true</visible>
 <uuid/>
 <bgcolor mode="nobackground">
  <r>255</r>
  <g>255</g>
  <b>255</b>
 </bgcolor>
 <bsbObject version="2" type="BSBGraph">
  <objectName/>
  <x>161</x>
  <y>195</y>
  <width>350</width>
  <height>150</height>
  <uuid>{e7c77635-a17a-4d20-9cda-67a1456a1a28}</uuid>
  <visible>true</visible>
  <midichan>0</midichan>
  <midicc>-3</midicc>
  <description/>
  <value>0</value>
  <objectName2>gieau</objectName2>
  <zoomx>1.00000000</zoomx>
  <zoomy>1.00000000</zoomy>
  <dispx>1.00000000</dispx>
  <dispy>1.00000000</dispy>
  <modex>lin</modex>
  <modey>lin</modey>
  <showSelector>true</showSelector>
  <showGrid>true</showGrid>
  <showTableInfo>true</showTableInfo>
  <showScrollbars>true</showScrollbars>
  <enableTables>true</enableTables>
  <enableDisplays>true</enableDisplays>
  <all>true</all>
 </bsbObject>
 <bsbObject version="2" type="BSBTableDisplay">
  <objectName/>
  <x>65</x>
  <y>100</y>
  <width>350</width>
  <height>150</height>
  <uuid>{37e06320-12fb-47db-9f13-126a1bc416db}</uuid>
  <visible>true</visible>
  <midichan>0</midichan>
  <midicc>-3</midicc>
  <description/>
  <color>
   <r>255</r>
   <g>193</g>
   <b>3</b>
  </color>
  <range>0.00</range>
 </bsbObject>
</bsbPanel>
<bsbPresets>
</bsbPresets>
