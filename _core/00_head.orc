;	GLOBAL VARIABLEs
gizero		= ksmps / sr	;i variable better than write zero? because it's between samples
gkzero		= ksmps / sr	;k variable better than write zero? because it's between samples

giexpzero	= .00015	;a zero value for expseg

gkgain		init 1		;master gain for "mouth" instrument

gkabstime	init 0

gienvdur	init 8192	;duration for all envelopes gen envelope tables
gioscildur	init 16384	;duration for all envelopes gen envelope tables
gigendur	init 8192



gidyn		init 1

;	MACROs

;	DYNAMICs
#define fff		#ampdb(-3)*gidyn#
#define ff		#ampdb(-7)*gidyn#
#define f		#ampdb(-9)*gidyn#
#define mf		#ampdb(-11)*gidyn#
#define mp		#ampdb(-15)*gidyn#
#define p		#ampdb(-21)*gidyn#
#define pp		#ampdb(-29)*gidyn#
#define ppp		#ampdb(-39)*gidyn#
#define pppp		#ampdb(-45)*gidyn#

print $fff
print $ff
print $f
print $mf
print $mp
print $p
print $pp
print $ppp

#define atk(atkms)	#+($atkms/1000)#

#define	ampvar		#(iamp+random:i(-$ppp, $ppp))#
#define env1		#a1*=envgen(idur-random:i(0, ienvvar), iftenv)#
#define env2		#a2*=envgen(idur-random:i(0, ienvvar), iftenv)#

;-----------------------------------------

;	HELPFULs

#define k		#*1000#
#define c		#*100#
#define d		#*10#

#define ms		#/1000#
#define s		#*1000#

#define if		#if (#
#define then		# == 1) then#

;-----------------------------------------

	instr	abstime

gkabstime	times
	
	endin
	alwayson("abstime")

;	generate date of the score
itim		date
Stim		dates     itim
Syear		strsub    Stim, 22, 24
Smonth		strsub    Stim, 4, 7
Sday		strsub    Stim, 8, 10
iday		strtod    Sday
Shor		strsub    Stim, 11, 13
Smin		strsub    Stim, 14, 16
Ssec		strsub    Stim, 17, 19
Sfilename	sprintf   "%s%s%02d_%s_%s_%s.orc", Syear, Smonth, iday, Shor,Smin, Ssec
gScorename	sprintf    "../_score/%s", Sfilename

;	OSC
gShost = "localhost"
giport = 10005
