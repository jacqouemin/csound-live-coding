;-----------------------------------------

indx	init 0

until indx == ginstrslen do
	isend	= 950 + (nstrnum(gSinstrs[indx])/1000)
	schedule isend, gizero, -1, gSinstrs[indx]
;	prints sprintf("\n%f clear %s\n", isend, gSinstrs[indx])
	indx	+= 1
od

;-----------------------------------------
;-----------------------------------------
;-----------------------------------------

	instr 900

Sinstr	= "mouth"

	prints("\n👅 My %s is open and ready to throw up or swallow 👅\n\n", Sinstr)

a1	chnget sprintf("%s-1", Sinstr)
a2	chnget sprintf("%s-2", Sinstr)

ilimit	=	.995

a1	limit a1, -ilimit, ilimit
a2	limit a2, -ilimit, ilimit

a1	*= portk(gkgain, 5$ms)
a2	*= portk(gkgain, 5$ms)

a1	dcblock2 a1
a2	dcblock2 a2

	outch giout1, a1, giout2, a2, girecout1, a2, girecout2, a1

;		SEND AUDIO to OSC
kwhen		metro 5

		OSCsend kwhen, gShost, giport, "/out1", "s", sprintfk("%f", k(a1))
		OSCsend kwhen, gShost, giport, "/out2", "s", sprintfk("%f", k(a2))

;		CLEAR
		chnclear sprintf("%s-1", Sinstr)
		chnclear sprintf("%s-2", Sinstr)

	endin
		alwayson(900)

;-----------------------------------------

	instr 950

;	the "clear" instrument

Sinstr	strget	p4
inum	nstrnum Sinstr

	prints("---%s(%i) is digested\n", Sinstr, inum)
	
	chnclear sprintf("%s-1", Sinstr)
	chnclear sprintf("%s-2", Sinstr)

	endin

;-----------------------------------------

	instr 955

ieach		= 4.5
ilen		lenarray gSinstrs

ktrig		init 0
ktrig		metro 1/ieach

kndx		init 0

kabstime	times

kmin		= int(kabstime/60)
ksec		= kabstime%60

if		ktrig == 1 then

		kndx	= 0

		until kndx == ilen do
			kactive	active gSinstrs[kndx]
			if (kactive > 45) then
				printks("Watch out! %i instaces of %s\n", 0, kactive, gSinstrs[kndx])
			endif
			kndx	+= 1
		od

		prints("\n-------checking instances-------\n", 0)

		printks("\n-------checking instances-------\n", 0)
		printks("--------- %.2d'  |  %.2d'' ---------\n", 0, kmin, ksec)

endif

	endin
	alwayson(955)

	instr 956

idiv 	init 1/96
kstart	init 1

if kstart == 1 then
	midiout 176, 1, 1, 2
endif

kstart = 0

;idiv 	init 1/96
;idiv	*= 64
kph	chnget	"heart"
kph	= (kph * 64 * 24) % 1

klast init -1
	
if (kph < klast) then
	midiout 176, 1, 1, 1
endif

klast = kph

	endin
	alwayson(956)
