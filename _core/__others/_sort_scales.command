#!/bin/bash

DIR="$(dirname "$0")"
FILE="$DIR"/scales.orc

sed -e 's/ ftgen /\t\tftgen\t\t/g' "$FILE" | sort -o "$FILE" "$FILE"