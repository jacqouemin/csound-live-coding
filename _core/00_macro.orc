;	DYNAMICs

#define fff	#ampdbfs(-3)#
#define ff	#ampdbfs(-5)#
#define f	#ampdbfs(-9)#
#define mf	#ampdbfs(-13)#
#define mp	#ampdbfs(-17)#
#define p	#ampdbfs(-23)#
#define pp	#ampdbfs(-29)#
#define ppp	#ampdbfs(-39)#

;-----------------------------------------

;	HELPFULs

#define k	#* 1000#
#define c	#* 100#
#define d	#* 10#

#define ms	#/ 1000#

#define if	#if (#
#define then	# == 1) then#

;-----------------------------------------
