	instr supersaw

Sinstr1		init "supersaw_instr1"
Sinstr2		init "supersaw_instr2"
Sinstr3		init "supersaw_instr3"

idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

indx		init 0
inum		init 7

while	indx<inum do
	schedule Sinstr1, 0, idur, iamp/2, iftenv, icps+gauss(icps/100)
	indx += 1
od
	schedule Sinstr1, 0, idur, iamp/2, iftenv, icps
	schedule Sinstr2, 0, idur, iamp/12, iftenv, icps/2
	schedule Sinstr3, 0, idur, iamp/16, iftenv, icps*2

		turnoff

	endin

	instr supersaw_instr1

Sinstr		init "supersaw"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

ift		init gisaw

kcps1		= icps + vibr(expseg(.05, idur, icps/(icps*12)), randomi:k(idur*3, idur*5, icps/(icps*12)), gisine)
kcps2		= icps + vibr(expseg(.05, idur, icps/(icps*12)), randomi:k(idur*3, idur*5, icps/(icps*12)), gisine)

a1		oscil3 iamp, kcps1, ift
a2		oscil3 iamp, kcps2, ift

;	ENVELOPE
ienvvar		init idur/10

a1		moogladder2 a1, (icps*9)+(envgen(idur-random:i(0, ienvvar), giclassic)*icps*15), randomi(.25, .65, 1/idur)
a2		moogladder2 a2, (icps*9)+(envgen(idur-random:i(0, ienvvar), giclassic)*icps*15), randomi(.25, .65, 1/idur)
 
$env1
$env2

;	ROUTING
S1		sprintf	"%s-1", Sinstr
S2		sprintf	"%s-2", Sinstr

		chnmix a1, S1
		chnmix a2, S2

	endin

	instr supersaw_instr2

Sinstr		init "supersaw"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

ift		init gisquare

kcps1		= icps + vibr(expseg(.05, idur, icps/(icps*12)), randomi:k(idur*3, idur*5, icps/(icps*12)), gisine)
kcps2		= icps + vibr(expseg(.05, idur, icps/(icps*12)), randomi:k(idur*3, idur*5, icps/(icps*12)), gisine)

a1		oscil3 iamp, kcps1, ift
a2		oscil3 iamp, kcps2, ift

;	ENVELOPE
ienvvar		init idur/10

$env1
$env2

;	ROUTING
S1		sprintf	"%s-1", Sinstr
S2		sprintf	"%s-2", Sinstr

		chnmix a1, S1
		chnmix a2, S2

	endin

	instr supersaw_instr3

Sinstr		init "supersaw"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

ift		init gisquare

kcps1		= icps + vibr(expseg(.05, idur, icps/(icps*12)), randomi:k(idur*3, idur*5, icps/(icps*12)), gisine)
kcps2		= icps + vibr(expseg(.05, idur, icps/(icps*12)), randomi:k(idur*3, idur*5, icps/(icps*12)), gisine)

a1		oscil3 iamp, kcps1, ift
a2		oscil3 iamp, kcps2, ift

;	ENVELOPE
ienvvar		init idur/10

$env1
$env2

;	ROUTING
S1		sprintf	"%s-1", Sinstr
S2		sprintf	"%s-2", Sinstr

		chnmix a1, S1
		chnmix a2, S2

	endin
