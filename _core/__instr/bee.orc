gibee_imp	ftgen 0, 0, 0, 1, "bee/bee02.wav", 0, 0, 0

	instr bee

Sinstr		init "bee"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

;		OSC1
ihard		init $ampvar	; the hardness of the stick used in the strike. A range of 0 to 1 is used. 0.5 is a suitable value
ipos		init $ampvar	; where the block is hit, in the range 0 to 1

imp		init gibee_imp

kvrate		expseg random:i(3, 5), idur/2, random:i(.25, .5)/idur
kvdepth		init iamp
ivibfn		init gisine

ai1		gogobel $ampvar/6, icps, ihard/6, ipos, imp, kvrate+random:i(-.05, .05), kvdepth, ivibfn
ai2		gogobel $ampvar/6, icps, ihard/6, ipos, imp, kvrate, kvdepth, ivibfn

;		OSC2
kc1		cosseg $ampvar*icps/(idur*50), idur, $ampvar*icps/(idur*100)
kc2		init 1

ai3		fmbell $ampvar, icps, kc1, kc2, kvdepth, kvrate, gisine, gisine, gisine, gitri, gisine, idur-random:i(0, idur/10)
ai4		fmbell $ampvar, icps, kc1, kc2, kvdepth, kvrate+random:i(-.05, .05), gisine, gisine, gisine, gitri, gisine, idur-random:i(0, idur/10)


ishapefn 	init gisaw

ai3   		table3 random:i(.05, .35)*ai3, ishapefn, 1, .5, 1
ai4   		table3 random:i(.05, .35)*ai4, ishapefn, 1, .5, 1

;		SUM
a1		sum ai1, ai3
a2		sum ai2, ai4

;		ENVELOPE
ienvvar		init idur/10

$env1
$env2

;		ROUTING
S1		sprintf	"%s-1", Sinstr
S2		sprintf	"%s-2", Sinstr

		chnmix a1, S1
		chnmix a2, S2

	endin
