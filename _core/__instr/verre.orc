gSverre_fold	init "verre/"

gSverre_1	init sprintf("%s%s.wav", gSverre_fold, "verre-001")
gSverre_2	init sprintf("%s%s.wav", gSverre_fold, "verre-002")
gSverre_3	init sprintf("%s%s.wav", gSverre_fold, "verre-003")
gSverre_4	init sprintf("%s%s.wav", gSverre_fold, "verre-004")
gSverre_5	init sprintf("%s%s.wav", gSverre_fold, "verre-005")
gSverre_6	init sprintf("%s%s.wav", gSverre_fold, "verre-006")

giverre_1len	filelen gSverre_1
giverre_2len	filelen gSverre_2
giverre_3len	filelen gSverre_3
giverre_4len	filelen gSverre_4
giverre_5len	filelen gSverre_5
giverre_6len	filelen gSverre_6

gSverre_end	init sprintf("%s%s.wav", gSverre_fold, "verre_end")

	instr verre

Sinstr		init "verre"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

Sfile		init gSverre_4
ilen		init giverre_4len

;		INSTR

iwave		init 1
ifn		init gisine
ipwvar		init .45
	
ai1		vco $ampvar, icps*random:i(1.995, 2.005)*.5, iwave, .5+random:i(-ipwvar, ipwvar), ifn
ai2		vco $ampvar, icps*random:i(1.995, 2.005)*.5, iwave, .5+random:i(-ipwvar, ipwvar), ifn

ai3		vco $ampvar*abs(lfo(1, 4/idur)), icps*random:i(1.995, 2.005)*2, iwave, .5+random:i(-ipwvar, ipwvar), ifn
ai4		vco $ampvar*abs(lfo(1, 4/idur)), icps*random:i(1.995, 2.005)*2, iwave, .5+random:i(-ipwvar, ipwvar), ifn

ai5		vco $ampvar*abs(lfo(1, 3/idur)), icps*random:i(1.995, 2.005)*4, 2, .5+random:i(-ipwvar, ipwvar), ifn
ai6		vco $ampvar*abs(lfo(1, 3/idur)), icps*random:i(1.995, 2.005)*4, 2, .5+random:i(-ipwvar, ipwvar), ifn

avco1		sum ai1, ai3/2, ai5/3
avco2		sum ai2, ai4/2, ai6/3

avco1		moogladder avco1, icps*100*(.15+$ampvar), .25
avco2		moogladder avco2, icps*100*(.15+$ampvar), .25

;		SAMPLE
istvar		init .0015

istart		init 0

as1, as2	diskin2 Sfile, random:i(.95, 1.05), istart+random:i(-istvar, istvar)
as3, as4	diskin2 Sfile, random:i(.95, 1.05), istart+random:i(-istvar, istvar)

adisk1		sum as1, as3
adisk2		sum as2, as4

adisk1		balance adisk1, ai1
adisk2		balance adisk2, ai2

;		OUTs

a1		= avco1
a2		= avco2

;		ENVELOPE
ienvvar		init idur/5

$env1
$env2

;	routing
S1		sprintf	"%s-1", Sinstr
S2		sprintf	"%s-2", Sinstr

		chnmix a1+adisk1, S1
		chnmix a2+adisk2, S2

	endin
