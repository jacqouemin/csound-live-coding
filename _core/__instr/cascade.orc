	instr cascade

Sinstr	= "cascade"
idur	init p3*2
iamp	init p4
iftenv	init p5
icps	init p6
ipan	init icps/100


asquare1 		poscil	1, expseg(icps, idur, icps+random:i(-ipan, ipan)), gisquare
asaw1			poscil	$ampvar, icps*asquare1, gisaw

asquare2 		poscil	1, expseg(icps, idur, icps+random:i(-ipan, ipan)), gisquare
asaw2			poscil	$ampvar, icps*asquare2, gisaw

adel1			linseg idur/48, idur, idur*random:i(3.95, 4)
adel2			linseg idur/48, idur, idur*random:i(3.95, 4)

kfb			expseg .015, idur, random:i(.5, .75)

a1			flanger asaw1, adel1, kfb
a2			flanger asaw2, adel2, kfb

;	ENVELOPE
ienvvar		init idur/5

$env1
$env2

/*
iatk	= idur * 1/32
idec	= idur * 3/32
isus	= random:i(.25, .35)
irel	= idur * 28/32

a1	*= expseg:a(.00015, iatk, 1, idec, isus, irel, .00015)
a2	*= expseg:a(.00015, iatk, 1, idec, isus, irel, .00015)
*/

;	ROUTING
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin
