	instr ilookintotheocean

Sinstr	= "ilookintotheocean"

idur	= p3
kcps	= p5
kamp	= p4

iarraypan[]	init 3
indx		init 0

while indx < 3 do
	iarraypan[indx]	= random(.35, .65)
	indx		+=	1
od

;FM_SQUARE
asquare 		poscil	1, kcps*1.05, gisquare
af1			=	kcps*asquare
aoutsquare		poscil	kamp / 3, af1, gisine
a1, a2			pan2	aoutsquare, iarraypan[0]

;FM_SAW
asaw			poscil	1, kcps*1.55, gisaw
af2			=	kcps*asaw
aoutsaw			poscil	kamp / 3, af1, gisine
a3, a4			pan2	aoutsaw, iarraypan[1]

;FM_TRI
atri			poscil	1, kcps*1.75, gitri
af3			=	kcps*atri
aouttri			poscil	kamp / 3, af3, gisine
a5, a6			pan2	aouttri, iarraypan[2]

a1			sum	a1, a3, a5
a2			sum	a2, a4, a6

;	envelope
iatk	= .065			; fix
idurenv	= idur - iatk
idec	= idurenv * 7/9		; 5 on 9
isus	= random:i(.05, .15)
irel	= idurenv * 2/9	 	; 4 on 9

a1	*= expseg:a(.00015, iatk, 1, idec, isus, irel, .00015)
a2	*= expseg:a(.00015, iatk, 1, idec, isus, irel, .00015)

;---routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin
