	instr puck

Sinstr		init "puck"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

ipanfreq	random -.25, .25

ifn		init 0
imeth		init 6

a1		pluck $ampvar, icps + randomi:k(-ipanfreq, ipanfreq, 1/idur), icps, ifn, imeth
a2		pluck $ampvar, icps*2 + randomi:k(-ipanfreq, ipanfreq, 1/idur), icps, ifn, imeth

;		ENVELOPE
ienvvar		init idur/10

$env1
$env2

;		ROUTING
S1		sprintf	"%s-1", Sinstr
S2		sprintf	"%s-2", Sinstr

		chnmix a1, S1
		chnmix a2, S2

	endin
