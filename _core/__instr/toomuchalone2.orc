	maxalloc "toomuchalone2_instr", 1

gktoomuchalone2_dur		init 1

gitoomuchalone2_morf		ftgen 0, 0, 3, -2, gisine, gitri, gisaw
gitoomuchalone2_dummy		ftgen 0, 0, gioscildur, 10, 1

	instr toomuchalone2

gktoomuchalone2_dur		= p3
gktoomuchalone2_amp		= p4
gktoomuchalone2_cps		= p6

	endin

	instr toomuchalone2_instr

Sinstr		init "toomuchalone2"

kport_amp	= gktoomuchalone2_dur/16
kport_cps	= gktoomuchalone2_dur/4
kport_env	= gktoomuchalone2_dur/4

kamp		= portk(gktoomuchalone2_amp, kport_amp)
kcps		= portk(gktoomuchalone2_cps, kport_cps)

acar1		oscil3 1, (kcps/2)+oscil3(kcps/100, kcps/randomi:k(95, 105, .05, 3), gisine), gisine
acar2		oscil3 1, (kcps/2)+oscil3(kcps/100, kcps/randomi:k(95, 105, .05, 3), gisine), gisine

kmorf		table3	phasor:k(kamp/gktoomuchalone2_dur), gieclassicr, 1

		ftmorf(kmorf, gitoomuchalone2_morf, gitoomuchalone2_dummy)

kmod		= kcps/randomi:k(35, 45, .05, 3)
kndx		= randomi:k(.25, .5, .05, 3)

ai1		foscil kamp, kcps+oscil3:k(kcps/100, kcps/randomi:k(95, 105, .05, 3), gisine), acar1, kmod, kndx, gitoomuchalone2_dummy
ai2		foscil kamp, kcps+oscil3:k(kcps/100, kcps/randomi:k(95, 105, .05, 3), gisine), acar2, kmod, kndx, gitoomuchalone2_dummy

kcf		table3	phasor:k(kamp/gktoomuchalone2_dur), gieclassic, 1
kres		= .95 * table3(phasor:k(kamp/gktoomuchalone2_dur), gifade, 1)

af1		moogladder2 ai1, (kcps/2)+(kcps*(kcf*randomi:k(16, 24, .05, 3))), kres
af2		moogladder2 ai2, (kcps/2)+(kcps*(kcf*randomi:k(16, 24, .05, 3))), kres

kenv		portk limit(abs(active:k("toomuchalone2")), 0, 1), kport_env

a1		= ((af1+ai1)/2)*kenv
a2		= ((af2+ai2)/2)*kenv

;		ROUTING
S1		sprintf	"%s-1", Sinstr
S2		sprintf	"%s-2", Sinstr

		chnmix a1, S1
		chnmix a2, S2

	endin
	start("toomuchalone2_instr")
