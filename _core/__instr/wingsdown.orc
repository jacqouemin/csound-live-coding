gkwingsdownmod		init 9			;mod parameter for witches instr
gkwingsdownndx		init 5			;index parameter for witches instr
gkwingsdowndetune	init 0		;detune parameter for witches instr

	instr wingsdown_instr

Sinstr		init "wingsdown"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6
indx		init p7

aamp1		= abs(lfo:a(iamp, linseg((1/idur)*random:i(12, 2), idur, (1/idur)*.5)))
aamp2		= abs(lfo:a(iamp, linseg((1/idur)*random:i(12, 2), idur, (1/idur)*.5)))

acar		= linseg:k(random:i(15, 9), idur/32, 1)

amod		= linseg:k(random:i(11, 7.5), idur/2, 7)
kndx		= linseg:k(random:i(indx, indx-1), idur/4, 1)

kvib1		vibr	expseg(.05, idur, icps/(icps*2)), randomi:k(idur*3, idur*5, icps/(icps*12)), gisine
kvib2		vibr	expseg(.05, idur, icps/(icps*2)), randomi:k(idur*3, idur*5, icps/(icps*12)), gisine

kcps1		= cosseg:k(icps*9/10, idur/4, icps) + kvib1
kcps2		= cosseg:k(icps*(9/10 + random:i(-.15, .15)), idur/4, icps) + kvib2

ift		init gisine

a1		foscili aamp1, kcps1, acar, amod, kndx, ift
a2		foscili aamp2, kcps2, acar, amod+randomi:a(-.0015, .0015, 1/idur, 2, 0), kndx+randomi:k(-.05, .05, 1/idur), ift

;	ENVELOPE
ienvvar		init idur/5

$env1
$env2

;	ROUTE
S1		sprintf	"%s-1", Sinstr
S2		sprintf	"%s-2", Sinstr

		chnmix a1, S1
		chnmix a2, S2

	endin



	instr wingsdown_control

Sinstr		init "wingsdown"

gkwingsdownmod		= xchan:k(sprintf("%s.mod", Sinstr), 9)
gkwingsdownndx		= xchan:k(sprintf("%s.ndx", Sinstr), 5)
gkwingsdowndetune	= xchan:k(sprintf("%s.detune", Sinstr), 0)

	endin
	alwayson("wingsdown_control")



	instr wingsdown

Sinstr		init "wingsdown_instr"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

indx	= i(gkwingsdownndx)
idetune = i(gkwingsdowndetune)

	schedule Sinstr, 0,	idur, iamp, iftenv, icps+idetune, indx

	turnoff

	endin
