gksuperclustermod		init 3 ;mod parameter for supercluster instr
gksuperclusterndx		init 11 ;index parameter for supercluster instr

gksuperclusterdetune		init 0 ;detune parameter for supercluster instr

	instr supercluster_instr

Sinstr		init "supercluster"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

//

indx		init p7
ift		init p8

//

kcar 		= 3
kmod 		= gksuperclustermod
kndx		= cosseg:k(indx, idur, indx/2)


a1		foscili $ampvar, icps, kcar, kmod, kndx, ift
a2		foscili $ampvar, icps+randomi:k(-.05, .05, 1/idur, 2, 0), kcar, kmod+randomi:k(-.015, .015, 1/idur, 2, 0), kndx+randomi:k(-.5, .5, 1/idur), ift

;	ENVELOPE
ienvvar		init idur/10

$env1
$env2

;	routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin

	instr supercluster_control

Sinstr			init "supercluster"

gksuperclustermod	= xchan:k(sprintf("%s.mod", Sinstr), 1)

gksuperclusterndx	= xchan:k(sprintf("%s.ndx", Sinstr), 11)

gksuperclusterdetune	= xchan:k(sprintf("%s.detune", Sinstr), 0)

	endin
	alwayson("supercluster_control")

	instr supercluster

Sinstr		init "supercluster_instr"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

indx	= i(gksuperclusterndx)

idetune = i(gksuperclusterdetune)

ichord	= .0095

	schedule Sinstr, random:i(0, ichord),	idur, iamp, iftenv, icps, indx, gisaw
	schedule Sinstr, random:i(0, ichord),	idur, iamp-$p, iftenv, icps*1.25+idetune, indx, gisquare
	schedule Sinstr, random:i(0, ichord),	idur, iamp-$mp, iftenv, icps*1.35+idetune*2, indx, gisine

	turnoff

	endin
