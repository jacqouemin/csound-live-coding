gkwingsupmod	init 9		;mod parameter for witches instr
gkwingsupndx	init 5		;index parameter for witches instr

gkwingsupdetune init 0		;detune parameter for witches instr

	instr wingsup_instr

Sinstr	= "wingsup"

idur 	= p3

aamp1	= abs(lfo:a(p4, linseg((1/idur)*random:i(12, 2), idur, (1/idur)*.5)))
aamp2	= abs(lfo:a(p4, linseg((1/idur)*random:i(12, 2), idur, (1/idur)*.5)))

icps 	= p5

indx	= p6

acar 	= linseg:k(random:i(15, 9), idur/32, 1)

amod 	= linseg:k(random:i(11, 7.5), idur/2, 7)
kndx	= linseg:k(random:i(indx, indx-1), idur/4, 1)

kvib1	vibr	expseg(.05, idur, icps/(icps*2)), randomi:k(idur*3, idur*5, icps/(icps*12)), gisine
kvib2	vibr	expseg(.05, idur, icps/(icps*2)), randomi:k(idur*3, idur*5, icps/(icps*12)), gisine

kcps1	= cosseg:k(icps*9/8, idur/4, icps) + kvib1
kcps2	= cosseg:k(icps*(9/8 + random:i(-.15, .15)), idur/4, icps) + kvib2

ift	= gisine

a1	foscili aamp1, kcps1, acar, amod, kndx, ift
a2	foscili aamp2, kcps2, acar, amod+randomi:a(-.0015, .0015, 1/idur, 2, 0), kndx+randomi:k(-.05, .05, 1/idur), ift

;	envelope
iatk	= idur * 1/32
idec	= idur * 13/32
isus	= random:i(.005, .05)
irel	= idur * 18/32

a1	*= expseg:a(.00015, iatk, 1, idec, isus, irel, .00015)
a2	*= expseg:a(.00015, iatk, 1, idec, isus, irel, .00015)

;	routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin

	instr wingsup_control

Sinstr	= "wingsup"

gkwingsupmod	= xchan:k(sprintf("%s.mod", Sinstr), 9)

gkwingsupndx	= xchan:k(sprintf("%s.ndx", Sinstr), 5)

gkwingsupdetune	= xchan:k(sprintf("%s.detune", Sinstr), 0)

	endin
	alwayson("wingsup_control")

	instr wingsup

Sinstr	= "wingsup_instr"

indx	= i(gkwingsupndx)

idetune = i(gkwingsupdetune)

	schedule Sinstr, 0,			p3, p4, p5+idetune,			indx

	turnoff

	endin
