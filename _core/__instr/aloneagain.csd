gkaloneagainactive	init 0
gkaloneagaincps		init 0

maxalloc "aloneagain_instr", 1

	instr aloneagain

Sinstr	= "aloneagain_instr"

idur	= p3
iamp	= p4
icps	= p5

	schedule "aloneagain_instr", gizero, idur, iamp, icps
	turnoff

	endin

;	---

	instr aloneagain_instr

Sinstr	= "aloneagain"

idur	= p3
iamp	= p4
icps	= p5

iport	= .5

;	ENV
iatk	= 1
idurenv	= idur - iatk
idec	= idurenv * .25
isus	= .5
irel	= idurenv * .75

aatk	linseg 0, iatk, 1
arel	linseg 1, irel, 0

;	INSTR management
gkaloneagainactive	active "aloneagain_instr"

;	---

if	i(gkaloneagainactive) == 0 then

	prints "TRUE\n"
	kcps		= icps
	gkaloneagaincps = icps
	
	aenv	= aatk
else
	prints "FALSE\n"
	kcps	linseg i(gkaloneagaincps), iport, icps
	gkaloneagaincps = icps

	aenv	= arel
endif

;	---

;	INSTR
a1	poscil iamp, kcps, gisine
a2	= a1

a1	*= aenv
a2	*= aenv

;	routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin
<bsbPanel>
 <label>Widgets</label>
 <objectName/>
 <x>0</x>
 <y>0</y>
 <width>0</width>
 <height>0</height>
 <visible>true</visible>
 <uuid/>
 <bgcolor mode="nobackground">
  <r>255</r>
  <g>255</g>
  <b>255</b>
 </bgcolor>
</bsbPanel>
<bsbPresets>
</bsbPresets>
