	instr bloch_instr
Sinstr	= "bloch"

idur	= p3
icps	= p5
iamp	= p4

kampdist = random:i(0, 5);choice of probability distribution for the next perturbation of the amplitude of a control point. The valid distributions are: 0 - LINEAR, 1 - CAUCHY, 2 - LOGIST, 3 - HYPERBCOS, 4 - ARCSINE, 5 - EXPON, 6 - SINUS (external k-rate signal) - If kampdist=6, the user can use an external k-rate signal through kadpar.
kdurdist = 1;choice of distribution for the perturbation of the current inter control point duration. See kampdist for the valid distributions. If kdurdist=6, the user can use an external k-rate signal through kddpar.
kadpar = .05;parameter for the kampdist distribution. Should be in the range of 0.0001 to 1.
kddpar = 1;parameter for the kdurdist distribution. Should be in the range of 0.0001 to 1.
kminfreq = icps-expseg(1, idur, 55);minimum allowed frequency of oscillation.
kmaxfreq = icps+expseg(1, idur, 5);maximum allowed frequency of oscillation.
kampscl = expseg(.005, idur, .5);multiplier for the distribution's delta value for amplitude (1.0 is full range).
kdurscl = 5;multiplier for the distribution's delta value for duration.
kcurveup = 5;controls the curve for the increasing amplitudes between two points; it has to be non negative.0: step function (like a sample & hold), <1: concave, 1: linear (like gendy), >1: convex
kcurvedown = .005;controls the curve for the decrasing amplitudes between two points; it has to be non negative.0: step function (like a sample & hold), <1: concave, 1: linear (like gendy), >1: convex

;	instr
a1	gendyx iamp, kampdist, kdurdist, kadpar, kddpar, kminfreq, kmaxfreq, kampscl, kdurscl, kcurveup, kcurvedown
a2	gendyx iamp, kampdist, kdurdist, kadpar, kddpar, kminfreq+random:i(-4.95, 4.95), kmaxfreq, kampscl, kdurscl, kcurveup, kcurvedown

;	envelope
iatk	= idur * 1/24
idec	= idur * 7/24
isus	= random:i(.05, .15)
irel	= idur * 16/24

;a1	*= mxadsr:a(iatk, idec, isus, irel)
;a2	*= mxadsr:a(iatk, idec, isus, irel)

a1	*= linseg:a(0, iatk, 1, idec, isus, irel, 0)
a2	*= linseg:a(0, iatk, 1, idec, isus, irel, 0)

;	routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin

	instr bloch

Sinstr	= "bloch_instr"

idur	= p3
icps	= p5
iamp	= p4/3

idetune	random	icps/5, icps/50

	schedule Sinstr, 0,	idur, iamp, icps
	schedule Sinstr, 0,	idur, iamp, icps-idetune
	schedule Sinstr, 0,	idur, iamp, icps+idetune
	schedule Sinstr, 0,	idur, iamp/4, icps*7

	turnoff

	endin
