		instr phrase

Sinstr		init "phrase"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

kcps		randomi icps/4, icps*4, cosseg(1/idur, idur, 500/idur), 2, icps

;		01 GLISS
ift1		init 8		;4 * x * (1 - x) (i.e. integrated sawtooth)

ai1		vco2	$ampvar, kcps, ift1
ai2		vco2	$ampvar, kcps+random:i(-icps/10, icps/10), ift1

ivib1		init random(1, 2.5)/3
ivib2		init random(3.5, 5)/3

ai1		= ai1/2 + ((ai1/2)*lfo(1, cosseg(ivib1+.05, idur/2, ivib2, idur/2, ivib1/2)))
ai2		= ai2/2 + ((ai2/2)*lfo(1, cosseg(ivib1, idur/2, ivib2, idur/2, ivib1/2)))

ai1		flanger ai1, expseg:a(, idur, idur/random(95, 105)), linseg(random(.45, .65), idur, .995)
ai2		flanger ai2, expseg:a(idur/random(95, 105), idur, idur/random(95, 105)), linseg(random(.45, .65), idur, .995)

;		02 GLISS
ift2		init 6		;sawtooth / triangle / ramp

ai3		vco2	$ampvar/2, 2*kcps+randomi:k(-icps/10, icps/10, random(1, 2.25)/idur), ift2, cosseg(.05, idur, random:i(.75, .95))
ai4		vco2	$ampvar/2, 2*kcps+randomi:k(-icps/10, icps/10, random(1, 2.25)/idur), ift2, cosseg(.05, idur, random:i(.75, .95))

ivib3		init random(1, 2.5)/4
ivib4		init random(3.5, 5)/4

ai3		= ai3/2 + ((ai3/2)*lfo(1, cosseg(ivib3, idur/2, ivib4, idur/2, ivib3/2)))
ai4		= ai4/2 + ((ai4/2)*lfo(1, cosseg(ivib3+.05, idur/2, ivib4, idur/2, ivib3/2)))

ai3		flanger ai3, expseg:a(idur/random(95, 105), idur, idur/random(95, 105)), linseg(random(.45, .65), idur, .995)
ai4		flanger ai4, expseg:a(idur/random(95, 105), idur, idur/random(95, 105)), linseg(random(.45, .65), idur, .995)

;---------------SUM
a1		sum ai1, ai3
a2		sum ai2, ai4

;		ENVELOPE
ienvvar		init idur/5

$env1
$env2

;		ROUTING
S1		sprintf	"%s-1", Sinstr
S2		sprintf	"%s-2", Sinstr

		chnmix a1, S1
		chnmix a2, S2

		endin
