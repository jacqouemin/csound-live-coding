gSmadcow	init "lab_amen_pack/SAMPLED AND REMIXED/165 Mad Cow Break.wav"

gimadcow1	ftgen 0, 0, 0, 1, gSmadcow, 0, 0, 1
gimadcow2	ftgen 0, 0, 0, 1, gSmadcow, 0, 0, 2

gkmadcow_time	init 1
gkmadcow_offset	init 35
gkmadcow_dur	init 1
;------------------

	instr madcow

Sinstr	init "madcow"
idur	init p3
idiv	init p4
iamp	init p5
ix	init p6

ioffset	init i(gkmadcow_offset)

korgan	chnget	"heart"
aorgan	interp korgan

aph1	= ((aorgan * idiv)+random:i(0, idur/ioffset)) % 1
aph2	= ((aorgan * idiv)+random:i(0, idur/ioffset)) % 1

a1	table3 aph1, gimadcow1, 1
a2	table3 aph2, gimadcow2, 1

a1	*= cosseg(0, ix, 1, p3-(ix*2), 1, ix, 0)
a2	*= cosseg(0, ix, 1, p3-(ix*2), 1, ix, 0)

a1	*= iamp
a2	*= iamp

;---routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin

;------------------

	opcode	madcow, 0, kk

kdiv, kamp xin

Sinstr	init "madcow"
imax	init 8
kndx 	init 1

if 	metro:k(gkbeatf*gkmadcow_time) == 1 then

	if kndx == imax+1 then
		kndx = 1
	endif

	kdur	= gkbeats*gkmadcow_dur

	kx	= kdur/4

	schedulek nstrnum(Sinstr)+(kndx/10), gkzero, kdur+kx, kdiv, kamp, kx
	kndx += 1
 
endif

	endop
