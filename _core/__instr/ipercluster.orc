giperfactor		init 	11

giperout		ftgen	0, 0, giperfactor,  -2, 0

giperpos		ftgen	0, 0, giperfactor*2, -41, 2, .25, 3, .15, 11, .5, 5, .45     
gipersnap		ftgen	0, 0, giperfactor*4, -41, 5, .25, 3, .15, 11, .5, 15, .45      

gipercluster_linvar	init giperfactor-1

	instr ipercluster

Sinstr		init "ipercluster_instr"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

	schedule Sinstr, 0, idur, iamp, iftenv, icps
	schedule Sinstr, 0, idur, iamp, iftenv, limit(icps/8, 20, 20$k)
	turnoff

	endin

	instr ipercluster_control

gkipercluster_linvar	= giperfactor/2 + lfo((giperfactor-1)/2, gkbeatf/16)
gkipercluster_vibfreq	randomh 2, 9, .5, 3

	endin
	alwayson("ipercluster_control")

	instr ipercluster_instr

Sinstr		init "ipercluster"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

ift		init gisaw

kx		expseg 1, idur/random(1, 1.25), random(giexpzero, giexpzero*100)

;              		kx,   inumParms,  inumPointsX,  iOutTab,  iPosTab,  iSnapTab  [, iConfigTab] 
	        hvs1    kx, 3, giperfactor, giperout, giperpos, gipersnap

#define ipercluster_var #tab:k(linseg(giexpzero, idur, giperfactor-random(1, i(gkipercluster_linvar))), giperout)#

ain1		oscil $ampvar, $ipercluster_var*icps, ift
ain2		oscil $ampvar, $ipercluster_var*icps, ift

krvt		cosseg 5/icps, idur, .05/icps
ilpt		init giexpzero*10 

a1		vcomb ain1, krvt, ilpt, 15
a2		vcomb ain2, krvt+(krvt/5), ilpt, 15

a1		*= vibr(.45, (gkipercluster_vibfreq+random(-.15, .15))/idur, gisine)
a2		*= vibr(.45, (gkipercluster_vibfreq+random(-.15, .15))/idur, gisine)

;		ENVELOPE
ienvvar		init idur/100

$env1
$env2

;	ROUTING
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin
