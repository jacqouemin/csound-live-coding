	instr inline

Sinstr		init "inline"

a1		inch 5
a2		= a1	

;		ROUTING
S1		sprintf	"%s-1", Sinstr
S2		sprintf	"%s-2", Sinstr

		chnmix a1, S1
		chnmix a2, S2

	endin
	alwayson("inline")
