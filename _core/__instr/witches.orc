gkwitchesmod	init 1 ;mod parameter for witches instr
gkwitchesndx	init 3 ;index parameter for witches instr
gkwitchesdetune init 0 ;detune parameter for witches instr

	instr witches_instr

Sinstr		init "witches"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6
indx		init p7

kcar 	= int(expseg:k(1, idur, limit(idur, 1, 7)))
amod 	a gkwitchesmod
kndx	= expseg:k(.05, idur, indx)

a1	foscili $ampvar, icps, kcar, amod, kndx, gisine
a2	foscili $ampvar, icps+randomi:k(-.05, .05, 1/idur, 2, 0), kcar, amod+randomi:a(-.0015, .0015, 1/idur, 2, 0), kndx+randomi:k(-.05, .05, 1/idur), gisine

;	ENVELOPE
ienvvar		init idur/10

$env1
$env2

;	routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin

	instr witches_control

Sinstr	= "witches"

gkwitchesmod	= xchan:k(sprintf("%s.mod", Sinstr), 1)
gkwitchesndx	= xchan:k(sprintf("%s.ndx", Sinstr), 3)
gkwitchesdetune	= xchan:k(sprintf("%s.detune", Sinstr), 0)

	endin
	alwayson("witches_control")

	instr witches

Sinstr		init "witches_instr"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

indx	= i(gkwitchesndx)
idetune = i(gkwitchesdetune)

	schedule Sinstr, 0,			p3, iamp, 	iftenv,		icps,			indx
	schedule Sinstr, random:i(0, p3),	p3, iamp/3,	iftenv,		icps*2+idetune, 	indx
	schedule Sinstr, random:i(0, p3),	p3, iamp/5,	iftenv, 	icps*3+idetune*2, 	indx

	schedule Sinstr, random:i(0, p3),	p3, iamp/12,	iftenv, 	icps*7+idetune*2, 	indx
	schedule Sinstr, random:i(0, p3),	p3, iamp/16,	iftenv, 	icps*11+idetune*3,	indx

	turnoff

	endin
