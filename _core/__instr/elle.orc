	instr elle

Sinstr	= "elle"

idur	= p3
icps	= p5
iamp	= p4

Sfile	= "opcode/elle.wav"
ilen	filelen Sfile

;	instr
istart	random	0, ilen - idur

a1, a2	diskin2 Sfile, random:i(.995, 1.005), istart
a3, a4	diskin2 Sfile, random:i(.995, 1.005), istart

;	envelope
idiv	= 16

a1	*= cosseg(0, .005, iamp, (idur - .005) * 8/idiv, iamp, (idur - .05) * 1/idiv, 0)
a4	*= cosseg(0, .005, iamp, (idur - .005) * 8/idiv, iamp, (idur - .05) * 1/idiv, 0)

;	routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a4, S2

	endin
