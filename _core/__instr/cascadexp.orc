	instr cascadexp_instr

Sinstr	= "cascadexp"

idur	init p3
iamp	init p4
iftenv	init p5
icps	init p6

ipan	init icps/95

iamp1	init $ampvar
iamp2 	init $ampvar

;			1st harmonic
asquare1 		poscil	1, expseg(icps, idur, icps+random:i(-ipan, ipan)), gisquare
asaw1			poscil	iamp1, icps*asquare1, gisine

asquare2 		poscil	1,  expseg(icps, idur, icps+random:i(-ipan, ipan)), gisquare
asaw2			poscil	iamp2, icps*asquare2, gisine

;			2nd harmonic
aharm2mod1		poscil	1, expseg(icps, idur, icps+random:i(-ipan, ipan)*4), gitri
aharm2out1		poscil	iamp1/32, icps*aharm2mod1*7, gisquare

aharm2mod2 		poscil	1,  expseg(icps, idur, icps+random:i(-ipan, ipan)*4), gitri
aharm2out2		poscil	iamp2/32, icps*aharm2mod2*7, gisquare

;			3rd harmonic
i1multi3		= 2
a2multi3		cosseg 24, idur, 12
kharm3amp		expseg 12, idur, 32

aharm3mod1		poscil	1, expseg(icps, idur, icps+random:i(-ipan, ipan)*i1multi3), gisquare
aharm3out1		poscil	iamp1/kharm3amp, icps*aharm2mod1*a2multi3, gisquare

aharm3mod2 		poscil	1,  expseg(icps, idur, icps+random:i(-ipan, ipan)*i1multi3), gisquare
aharm3out2		poscil	iamp2/kharm3amp, icps*aharm2mod2*a2multi3, gisquare

;			total
asyn1			sum	asaw1, aharm2out1, aharm3out1
asyn2			sum	asaw2, aharm2out2, aharm3out2

adel1			expseg .0015, idur, random:i(.075, .0075)
adel2			expseg .0015, idur, random:i(.075, .0075)

kfb1			expseg random:i(.75, .705), idur, random:i(.15, .25)
kfb2			expseg random:i(.75, .705), idur, random:i(.15, .25)

a1			flanger asyn1, adel1, kfb1
a2			flanger asyn1, adel2, kfb2

a1			buthp a1, icps - icps/12
a2			buthp a2, icps - icps/12

;	ENVELOPE
ienvvar		init idur/10

$env1
$env2

/*

iatk			= .115

idurenv			= idur - iatk
idec			= idurenv * 1/32
isus1			= random:i(.05, .15)
irel1			= idurenv * 3/32

isus2			= random:i(.015, .05)
irel2			= idurenv * 28/32

a1			*= linseg:a(0, iatk, 1, idec, isus1, irel1, isus2, irel2, 0)
a2			*= linseg:a(0, iatk, 1, idec, isus1, irel1, isus2, irel2, 0)
*/

;	routing
S1			sprintf	"%s-1", Sinstr
S2			sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin

	instr cascadexp

Sinstr	= "cascadexp_instr"

iarp	= .0095

	schedule Sinstr, random:i(0, iarp),	p3, p4, p5, p6
	schedule Sinstr, random:i(0, iarp),	p3, p4, p5, p6
	schedule Sinstr, random:i(0, iarp),	p3, p4, p5, p6

	turnoff

	endin
