giohvar		init -1

	instr oh

S1		init "oh_instr1"
S2		init "oh_instr2"

idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

i3m		init (icps*6/5)-icps
i3cps		init icps+(i3m*giohvar)

		event_i "i", S1, 0, 5$ms, iamp, icps
		event_i "i", S2, 0, idur, iamp, iftenv, i3cps

giohvar		*= -1

		turnoff

	endin

	instr oh_instr1

Sinstr		init "oh"
idur		init p3
iamp		init p4
icps		init p5

ift		init gitri
ivar		init icps/10

a1		oscil $ampvar, icps, ift
a2		oscil $ampvar, icps, ift

;		ENVELOPE
a1		*= cosseg(0, idur/2, 1, idur/2, 0)
a2		*= cosseg(0, idur/2, 1, idur/2, 0)

;		ROUTING
S1		sprintf	"%s-1", Sinstr
S2		sprintf	"%s-2", Sinstr

		chnmix a1, S1
		chnmix a2, S2

	endin

	instr oh_instr2

Sinstr		init "oh"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

ift		init gitri

ivar		init icps/10

a1		oscil $ampvar, icps, ift
a2		oscil $ampvar, icps+gauss(3), ift

;		ENVELOPE
ienvvar		init idur/10

$env1
$env2

;		ROUTING
S1		sprintf	"%s-1", Sinstr
S2		sprintf	"%s-2", Sinstr

		chnmix a1, S1
		chnmix a2, S2

	endin
