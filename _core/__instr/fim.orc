gkfimf1		init 0
gkfimf2		init 0

	instr fim_control

gafimf1		randomi -5, 5, .05, 3
gafimf2		randomi -5, 5, .05, 3		

	endin
	alwayson("fim_control")

	instr fim

Sinstr		init "fim"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

afreq		= icps*poscil(1, icps*1.95, gisaw)

as1		poscil	$ampvar, afreq+gafimf1, gisine
as2		poscil	$ampvar, afreq+gafimf2, gisine

iff		= limit(icps-(icps/9), 20, 20$k)

a1		atonex	as1, iff
a2		atonex	as2, iff

a1		balance a1, as1
a2		balance a2, as2

;	ENVELOPE
ienvvar		init idur/10

$env1
$env2

;	routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin
