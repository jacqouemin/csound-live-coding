	instr	clicka

Sinstr		init "clicka_instr"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

	schedule Sinstr, 0, idur, iamp, iftenv, icps
	turnoff 

	endin

	maxalloc "clicka_instr", 1

	instr clicka_instr

Sinstr		init "clicka"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

ain		fractalnoise iamp, .95

krvt		= .05

ilpt		init giexpzero
klpt		= 1/icps

a1		vcomb ain, krvt, klpt, ilpt
a2		vcomb ain, krvt, klpt, ilpt

a1		balance a1, ain*$ampvar
a2		balance a2, ain*$ampvar

;		ENVELOPE
ienvvar		init idur/10

$env1
$env2

;		ROUTING
S1		sprintf	"%s-1", Sinstr
S2		sprintf	"%s-2", Sinstr

		chnmix a1, S1
		chnmix a2, S2

	endin
	alwayson("clicka_instr")
