	instr eight

Sinstr	= "eight"

idur	= p3
iamp	= p4
icps	= p5

kamp	= icps/128
kfreq	expseg .5/idur, idur, 6/idur

kcps	vibr kamp, kfreq, gisine

;	instr

iwave	= gisine

a1	poscil 1, icps+kcps, iwave
a2	poscil 1, icps+kcps+random:i(0, 1), iwave

;	ENV
iatk	= 25$ms
idurenv	= idur - iatk
idec	= idurenv * 2/5
isus	random .15, .25
irel	= idurenv * 3/5

aenv	linseg	0, iatk, iamp, idec, isus, irel, 0 

a1	*= aenv
a2	*= aenv

;	ROUTING
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin
