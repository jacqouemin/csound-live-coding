	instr infinite

Sinstr	= "infinite_instr"

inum	init 0
imax	init 128

ilimit limit p3, gizero, gizero*imax

while	inum < imax do
	schedule Sinstr, random(0, ilimit*8), ilimit, p4/imax, p5
	inum += 1
od
	turnoff

	endin

	instr infinite_instr

Sinstr	= "infinite"

idur	= p3
iamp	= p4
icps	= p5

;	INSTR
anoi	fractalnoise random(.05, .5), random(.05, .5)

;	ENV
iatk	= idur * .005
idec	= idur * .005
isus	= .25
irel	= idur * .995

aenv1	expseg gizero, iatk, iamp, idec, random(isus, isus+.15), irel, gizero
aenv2	expseg gizero, iatk, iamp, idec, random(isus, isus+.15), irel, gizero

a1	= anoi * aenv1
a2	= anoi * aenv2

;	routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin

