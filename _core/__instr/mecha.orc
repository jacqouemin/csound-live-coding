	instr mecha

Sinstr		init "mecha"

idur		init p3
iamp		init p4
iftenv		init p5
ifile		init p6

if	ifile>=1 && ifile<5 then

	if	(ifile == 1) then
		Sfile	= "diskclavier/mecha_1.wav"
	elseif	(ifile == 2) then
		Sfile	= "diskclavier/mecha_2.wav"
	elseif	(ifile == 3) then
		Sfile	= "diskclavier/mecha_3.wav"
	elseif	(ifile == 4) then
		Sfile	= "diskclavier/mecha_4.wav"

	endif

	ilen	filelen Sfile

	;		INSTR
	idur	limit	idur, .005, ilen-.005
	istart	random	0, ilen - idur

	ivar	init	.05		

	a1, a2	diskin2 Sfile, 1+random:i(-ivar, ivar), istart
	a3, a4	diskin2 Sfile, 1+random:i(-ivar, ivar), istart

	;		ENVELOPE
	ienvvar		init idur/10

	$env1
	$env2

	;		ROUTE
		S1	sprintf	"%s-1", Sinstr
		S2	sprintf	"%s-2", Sinstr

			chnmix a1, S1
			chnmix a4, S2

endif

	endin
