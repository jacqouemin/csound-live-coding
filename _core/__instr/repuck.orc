	instr repuck

Sinstr		init "repuck"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

ipanfreq	init random:i(-.25, .25)

a1	repluck random:i(.015, .35), $ampvar, icps + random:i(-ipanfreq, ipanfreq), randomh:k(.25, .95, random:i(.05, .15)), random:i(.05, .65), poscil(1, random:i(.05, .25),  gisine)
a2	repluck random:i(.015, .35), $ampvar, icps + random:i(-ipanfreq, ipanfreq), randomh:k(.25, .95, random:i(.05, .15)), random:i(.05, .65), poscil(1, random:i(.05, .25),  gisine)

;	ENVELOPE
ienvvar		init idur/10

$env1
$env2

a1	buthp a1, icps - icps/12
a2	buthp a2, icps - icps/12

;	routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin
