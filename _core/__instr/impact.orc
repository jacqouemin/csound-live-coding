	instr impact

S_instr		init "impact_instr"
S_route		init "impact_route"

idur		random p3, p3+5

		schedule S_instr, 0, idur
		schedule nstrnum(S_route)+.15, 0, idur

		turnoff

	endin
	
	instr impact_instr

idur	init p3

	schedule("oh",
	random(gizero, gizero*100),
	random(idur*2/3, idur),
	$mf,
	gieclassic$atk(5),
	random(30, 152))

	schedule("gliss",
	random(gizero, gizero*100),
	random(idur*2/3, idur),
	$mf,
	gieclassic$atk(5),
	random(530, 5252))

	schedule("aaron",
	random(gizero, gizero*100),
	random(idur*2/3, idur),
	$mp,
	gilikearev,
	random(30, 1200))

	schedule("click",
	random(gizero, gizero*100),
	random(idur*2/3, idur),
	$mp,
	gieclassic,
	random(30, 420))

	schedule("aaron",
	random(gizero, gizero*100),
	random(idur*2/3, idur),
	$mp,
	giclassic,
	random(30, 3200))

	schedule("ipercluster",
	random(gizero, gizero*100),
	random(idur*2/3, idur),
	$ppp,
	gieclassic$atk(5),
	random(30, 5200))

	schedule("click",
	random(gizero, gizero*100),
	random(idur*2/3, idur),
	$mp,
	gieclassic,
	random(30, 1200))

	endin


	instr impact_route

getmeout("aaron")
getmeout("click")
getmeout("oh")
getmeout("ipercluster")
getmeout("gliss")

	endin
