	instr repuckf

Sinstr		init "repuckf"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

ipanfreq	init random:i(-.25, .25)

kamp1	= abs(lfo:a($ampvar, cosseg(random:i(idur*.5, idur*.75)/2, idur, random:i(idur*.75, idur*3.5)/2)))
kamp2	= abs(lfo:a($ampvar, cosseg(random:i(idur*.5, idur*.75)/2, idur, random:i(idur*.75, idur*3.5)/2)))

kcps1	= icps + vibr(expseg(.05, idur, icps/(icps*12)), randomi:k(idur*3, idur*5, icps/(icps*12)), gisine)
kcps2	= icps + vibr(expseg(.05, idur, icps/(icps*12)), randomi:k(idur*3, idur*5, icps/(icps*12)), gisine)

ain1	repluck random:i(.015, .35), kamp1, icps + random:i(-ipanfreq, ipanfreq), randomh:k(.25, .95, random:i(.05, .15)), random:i(.05, .65), poscil(kamp1/2, kcps1 + random:i(-ipanfreq, ipanfreq),  gisaw)
ain2	repluck random:i(.015, .35), kamp2, icps + random:i(-ipanfreq, ipanfreq), randomh:k(.25, .95, random:i(.05, .15)), random:i(.05, .65), poscil(kamp2/2, kcps2 + random:i(-ipanfreq, ipanfreq),  gisaw)

a1	flanger ain1, cosseg:a(random:i(.0015, .0025), idur, random:i(.0025, .005)), expseg:k(random:i(.65, .85), idur, random:i(.85, .95))
a2	flanger ain2, cosseg:a(random:i(.0015, .0025), idur, random:i(.0025, .005)), expseg:k(random:i(.65, .85), idur, random:i(.85, .95))

;	ENVELOPE
ienvvar		init idur/10

$env1
$env2

a1	buthp a1, icps - icps/12
a2	buthp a2, icps - icps/12

;	routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin
