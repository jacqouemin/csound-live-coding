	instr fairest

Sinstr	= "fairest"

idur	= p3
icps	= p5
iamp	= p4

ivib		= $p
kvibfreq	= randomi:k(1.5, 5, .25, 3)
iamp		-= ivib

;	a1
atri1	poscil	1, icps + random:i(-1, 1), gitri
asin1	poscil	1/4, icps*3 + random:i(-1, 1), gisine
asqu1	poscil	1/12, icps/2 + random:i(-1, 1), gisquare

aharm1	poscil	1/8, icps*6/2 + random:i(-1, 1), gisquare


a1	sum	atri1, asin1, asqu1, aharm1
a1	/= 4

ad1	distort a1, expseg:k(random:i(.85, .15), idur, .05), gitri
af1	moogladder ad1, icps + expseg:k(icps*8 + random:i(-15, 15), idur, 0.05), expseg:k(.5, idur*9/10, random:i(.895, .65), idur/10, .25)
aout1	K35_hpf af1, 25, 7.5

aout1	*= iamp + (lfo:a(ivib, kvibfreq + random:i(-.15, .15)) * expsega(.0005, idur, 1))

;	envelope
idiv	= random:i(9, 15)

iatk	= idur*((idiv-8)/idiv)
idec	= idur*((idiv-5)/idiv)
isus	= random:i(.25, .5)
irel	= idur*((idiv-4)/idiv)

aout1	*= expseg:a(.00015, iatk, 1, idec, isus, irel, .00015)
a1	= aout1

;	a2
atri2	poscil	1, icps + random:i(-1, 1), gitri
asin2	poscil	1/4, icps*3 + random:i(-1, 1), gisine
asqu2	poscil	1/12, icps/2 + random:i(-1, 1), gisquare

aharm2	poscil	1/8, icps*6/2 + random:i(-1, 1), gisquare


a2	sum	atri2, asin2, asqu2, aharm2
a2	/= 4

ad2	distort a2, expseg:k(random:i(.85, .15), idur, .05), gitri
af2	moogladder ad2, icps + expseg:k(icps*8 + random:i(-15, 15), idur, 0.05), expseg:k(.5, idur*9/10, random:i(.895, .65), idur/10, .25)
aout2	K35_hpf af2, 25, 7.5

aout2	*= iamp + (lfo:a(ivib, kvibfreq + random:i(-.15, .15)) * expsega(.0005, idur, 1))

;	envelope
idiv	= random:i(9, 15)

iatk	= idur*((idiv-8)/idiv)
idec	= idur*((idiv-5)/idiv)
isus	= random:i(.25, .5)
irel	= idur*((idiv-4)/idiv)

aout2	*= expseg:a(.00015, iatk, 1, idec, isus, irel, .00015)
a2	= aout2

;	routing
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin
