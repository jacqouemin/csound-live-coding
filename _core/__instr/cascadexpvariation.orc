	instr cascadexpvariation_instr

Sinstr	= "cascadexpvariation"

/*
;			1st harmonic
asquare1 		poscil	1, expseg(icps, idur, icps+random:i(-ipan, ipan)), giSquare
asaw1			poscil	iamp, icps*asquare1, giSine

asquare2 		poscil	1,  expseg(icps, idur, icps+random:i(-ipan, ipan)), giSquare
asaw2			poscil	iamp, icps*asquare2, giSine

;			2nd harmonic
aharm2mod1		poscil	1, expseg(icps, idur, icps+random:i(-ipan, ipan)*4), giTri
aharm2out1		poscil	iamp/32, icps*aharm2mod1*7, giSquare

aharm2mod2 		poscil	1,  expseg(icps, idur, icps+random:i(-ipan, ipan)*4), giTri
aharm2out2		poscil	iamp/32, icps*aharm2mod2*7, giSquare

;			3rd harmonic
i1multi3		= 2
a2multi3		cosseg 24, idur, 12
kharm3amp		expseg 12, idur, 32

aharm3mod1		poscil	1, expseg(icps, idur, icps+random:i(-ipan, ipan)*i1multi3), giSquare
aharm3out1		poscil	iamp/kharm3amp, icps*aharm2mod1*a2multi3, giSquare

aharm3mod2 		poscil	1,  expseg(icps, idur, icps+random:i(-ipan, ipan)*i1multi3), giSquare
aharm3out2		poscil	iamp/kharm3amp, icps*aharm2mod2*a2multi3, giSquare
*/

;			OSCIL
idur			init p3
iamp			init p4
kamp			= (p6 == 1 ? 1 : expseg(p6, idur, p6/2))
icps			init p5
ivarcps			init icps/random:i(95, 105)

aharm			= p7
imodharm		init 1
iftmod			init p9
iftoscil		init p10

amod1			poscil	1, linseg(icps, idur, icps+random:i(-ivarcps, ivarcps)), iftmod
a1			poscil	iamp/kamp, icps*amod1*aharm, iftoscil

amod2			poscil	1, linseg(icps, idur, icps+random:i(-ivarcps, ivarcps)), iftmod
a2			poscil	iamp/kamp, icps*amod2*aharm, iftoscil

;			FLANGER
adel1			expseg .0015, idur, random:i(.075, .0075)
adel2			expseg .0015, idur, random:i(.075, .0075)

kfb1			expseg random:i(.75, .705), idur, random:i(.15, .25)
kfb2			expseg random:i(.75, .705), idur, random:i(.15, .25)

a1			flanger a1, adel1, kfb1
a2			flanger a2, adel2, kfb2

;			FILTER
a1			buthp a1, icps - icps/12
a2			buthp a2, icps - icps/12

;			ENVELOPE
iatk			= .115

idurenv			= idur - iatk
idec			= idurenv * 1/32
isus1			= random:i(.05, .15)
irel1			= idurenv * 3/32
isus2			= random:i(.015, .05)
irel2			= idurenv * 28/32

a1			*= linseg:a(0, iatk, 1, idec, isus1, irel1, isus2, irel2, 0)
a2			*= linseg:a(0, iatk, 1, idec, isus1, irel1, isus2, irel2, 0)

;	routing
S1			sprintf	"%s-1", Sinstr
S2			sprintf	"%s-2", Sinstr

			chnmix a1, S1
			chnmix a2, S2

	endin

	instr cascadexpvariation

Sinstr	= "cascadexpvariation_instr"

iarp	= .0095

;							p6		p7		p8		p9		p10	
;							ampdiv		harm		modharm		iftmod		iftoscil	
schedule Sinstr, random:i(0, iarp),	p3, p4,	p5,	1,		1,		1,		gisquare,	gisine	
schedule Sinstr, random:i(0, iarp),	p3, p4, p5,	6,		4, 		7,		gitri,		gisquare
schedule Sinstr, random:i(0, iarp),	p3, p4, p5,	12,		2, 		24, 		gisquare,	gisquare

	turnoff

	endin
