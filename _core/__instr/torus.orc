	instr torus

Sinstr		init "torus"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

;	   p8
;1024 .05  -1 -1  0.00667    0.000651    10 -1 .856 1.1 1 .06 51.2 -20.200590133667 .172539323568344 -4.07686233520508 2.5 10 .2 1 66 81 ; periodic attractor (torus breakdown route)

istepsize	init icps/1000
iL		init .065
iR0		init .00065
iC2		init 10
iG		init -1
iGa		init .85
iGb		init 1.15
iE		init 1
iC1		init .65
iI3		init -20.25
iV2		init .1725 
iV1		init -4.076862335

aguide		buzz    $ampvar, 500, sr/500, gisine

aI3, aV2, aV1	chuap   iL, iR0, iC2, iG, iGa, iGb, iE, iC1, iI3, iV2, iV1, istepsize
 
a1		balance aI3, aguide
a2		balance aI3, aguide

;	ENVELOPE
ienvvar		init idur/10

$env1
$env2

;	ROUTING
S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

	chnmix a1, S1
	chnmix a2, S2

	endin
