	instr wendj

Sinstr		init "wendj"
idur		init p3
iamp		init p4
iftenv		init p5
icps		init p6

;	gendy kamp, kampdist, kdurdist, kadpar, kddpar, kminfreq, kmaxfreq, kampscl, kdurscl [, initcps] [, knum]

kampdist	init 1
kdurdist	init 0

kadpar		init .995 ;parameter for the kampdist distribution. Should be in the range of 0.0001 to 1
kddpar		init .45 ;parameter for the kdurdist distribution. Should be in the range of 0.0001 to 1

kvar		expseg icps/75, idur, icps/15

kminfreq	= icps-kvar
kmaxfreq	= icps+kvar

kampscl		init .95 ;multiplier for the distribution's delta value for amplitude (1.0 is full range)
kdurscl		init .15 ;multiplier for the distribution's delta value for duration

initcps		init 16+(iamp*32)

;		INSTR
ai1			gendy $ampvar, kampdist, kdurdist, kadpar, kddpar, kminfreq, kmaxfreq, kampscl, kdurscl, initcps
ai2			gendy $ampvar, kampdist, kdurdist, kadpar, kddpar, kminfreq, kmaxfreq, kampscl, kdurscl, initcps

;		INSTR 2
ai3_1		oscil $ampvar, icps*random:i(1.995, 2.005)*2, gisaw
ai4_1		oscil $ampvar, icps*random:i(1.995, 2.005)*2, gisaw

ai3_2		oscil $ampvar, icps*random:i(1.995, 2.005)*4, gisaw
ai4_2		oscil $ampvar, icps*random:i(1.995, 2.005)*4, gisaw

ai3_o1		sum ai3_1, ai4_1/1.75
ai3_o2		sum ai3_2, ai4_2/1.75

iatk		init idur*.05
idec		init idur*.45
isus		init random(.35, .45)
irel		init idur*.5

#define 	wendjvib2 #abs(oscil(1, cosseg(random(.5, 1)/iatk, idur, random(5, 45)/irel), gisine))#

ai3		= ai3_o1*linseg(0, iatk, 1, idec, isus, irel, 0) * $wendjvib2
ai4		= ai3_o2*linseg(0, iatk, 1, idec, isus, irel, 0) * $wendjvib2

;		INSTR 3
ai5		oscil $ampvar, icps*random:i(1.995, 2.005)/2, gitri
ai6		oscil $ampvar, icps*random:i(1.995, 2.005)/2, gitri

ai5		*= cosseg(1, idur/(3+gauss(.25)), 0)
ai6		*= cosseg(1, idur/(3+gauss(.25)), 0)

;		MIX
a1		sum ai1, ai3*.65, ai5*1.35
a2		sum ai2, ai4*.65, ai6*1.35

;a1		= ai3
;a2		= ai4

;		ENVELOPE
ienvvar		init idur/10

$env1
$env2

;		ROUTE
S1		sprintf	"%s-1", Sinstr
S2		sprintf	"%s-2", Sinstr

		chnmix a1, S1
		chnmix a2, S2

	endin
