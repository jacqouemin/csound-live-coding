#!/bin/bash

DIR="$(dirname "$0")"
FILE_INCLUDE="$DIR"/_livecode-include.csd
FILE_LIVECODING="$DIR"/_livecode.csd
FILE_SCORE="$DIR"/_score.csd

> "$FILE_INCLUDE"

cp -p "$DIR"/_livecode-settings.csd "$FILE_LIVECODING"

#	MAIN FOLDER
find "$DIR" -maxdepth 1 -type f -name "*.orc" |
sed -e 's/^/#include "/' |
sed -e 's/$/"/' | 
sort >> "$FILE_INCLUDE"

#	GENs
find "$DIR"/__gen -type f -name "*.orc" |
sed -e 's/^/#include "/' |
sed -e 's/$/"/' | 
sort >> "$FILE_INCLUDE"

#	OPCODEs
find "$DIR"/__opcode -type f -name "*.orc" |
sed -e 's/^/#include "/' |
sed -e 's/$/"/' | 
sort >> "$FILE_INCLUDE"

#	FXs
find "$DIR"/__fx -type f -name "*.orc" |
sed -e 's/^/#include "/' |
sed -e 's/$/"/' | 
sort >> "$FILE_INCLUDE"

#	INSTRs
find "$DIR"/__instr -type f -name "*.orc" |
sed -e 's/^/#include "/' |
sed -e 's/$/"/' | 
sort >> "$FILE_INCLUDE"

#	OTHERs
find "$DIR"/__others -type f -name "*.orc" |
sed -e 's/^/#include "/' |
sed -e 's/$/"/' | 
sort >> "$FILE_INCLUDE"


echo -e "\n#include \""$DIR"/_livecode-include.csd\"" >> "$FILE_LIVECODING"

cp "$DIR"/_livecode.csd "$FILE_SCORE"

echo -e "\n</CsInstruments>\n</CsoundSynthesizer>" >> "$FILE_LIVECODING"

echo -e "\n</CsInstruments>\n<CsScore>\n</CsScore>\n</CsoundSynthesizer>" >> "$FILE_SCORE"

echo -e -n 'gSinstrs[]\tfillarray\t' > "$DIR"/00_instruments.orc # -n delete adding in new line

find "$DIR"/__instr -type f -name "*.orc" -exec basename {} \; | cut -f 1 -d '.' | grep -v "_.*" | sort | sed -e 's/^/"/' | sed -e 's/$/"/' | perl -pe 's/\n/$1, /' | sed 's/, $//' >> "$DIR"/00_instruments.orc

echo -e -n '\nginstrslen\tlenarray\tgSinstrs\n' >> "$DIR"/00_instruments.orc

exit