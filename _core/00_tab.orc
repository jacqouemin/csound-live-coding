
;	OSCIL GEN
gisine		ftgen	0, 0, gioscildur, 10, 1					; sine wave
gisquare	ftgen	0, 0, gioscildur, 7, 1, gioscildur/2, 1, 0, -1, gioscildur/2, -1		; square wave 
gitri		ftgen	0, 0, gioscildur, 7, 0, gioscildur/4, 1, gioscildur/2, -1, gioscildur/4, 0		; triangle wave 
gisaw		ftgen	0, 0, gioscildur, 7, 1, gioscildur, -1				; sawtooth wave, downward slope

;	TIME GEN
girall		ftgen	0, 0, 16384, 5, giexpzero, 14500, .15, 1500, 1, 385, 1
giacc		ftgen	0, 0, 16384, 5, 1, 1500, .15, 1, 14500, giexpzero
gilinear	ftgen	0, 0, 16384, 7, 0, 16384, 1

;	LFOs GEN
gilowsine	ftgen	0, 0, gigendur, 10, 1				; sine wave
gilowsquare	ftgen	0, 0, gigendur, 7, 1, gigendur/2, 1, 0, -1, gigendur/2, -1	; square wave 
gilowtri	ftgen	0, 0, gigendur, 7, 0, gigendur/4, 1, gigendur/2, -1, gigendur/4, 0	; triangle wave 
gilowsaw	ftgen	0, 0, gigendur, 7, 1, gigendur, -1			; sawtooth wave, downward slope

gilowasine	ftgen	0, 0, gigendur, -24, gilowsine, 0, 1		; sine wave
gilowasquare	ftgen	0, 0, gigendur, 7, 1, gigendur/2, 1, 0, 0, gigendur/2		; square wave 
gilowatri	ftgen	0, 0, gigendur, 7, 0, gigendur/2, 1, gigendur/2, 0		; triangle wave 
gilowasaw	ftgen	0, 0, gigendur, 7, 1, gigendur, 0			; sawtooth wave, downward slope
