	instr k35

;	variables
Sinstr	= "k35"

S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

Sp1	= sprintf("%s.freq", Sinstr)
Sp2	= sprintf("%s.q", Sinstr)

;	routing
a1	chnget S1
a2	chnget S2

kcf	= xchan:k(Sp1, 300)
kq	= xchan:k(Sp2, .25)

kq	*= 10
	
;	instrument
af1	K35_lpf a1, kcf, kq
af2	K35_lpf a2, kcf, kq
	
aout1	balance	af1, a1
aout2	balance	af2, a2

;	out
	chnmix aout1, "mouth-1"
	chnmix aout2, "mouth-2"
;	out
	chnmix af1, "mouth-1"
	chnmix af2, "mouth-2"

;	clearchns
	chnclear S1
	chnclear S2

	endin

;	alwayson
	alwayson("k35")
