	instr shy

;	variables
Sinstr	= "shy"

S1	sprintf	"%s-1", Sinstr
S2	sprintf	"%s-2", Sinstr

;	routing
ain1	chnget S1
ain2	chnget S2

;	INSTR

;	INIT
afb1 	init	0 
afb2 	init	0 
ifblvl 	init	.95 

; pre-delay
idel	=	100
adel	=	idel

a1 	=	vdelay3(ain1, adel, idel) 
a2 	=	vdelay3(ain2, adel, idel) 
 
a1 	=	a1 + (afb1 * ifblvl) 
a2 	=	a2 + (afb2 * ifblvl) 

;a1	= afb1*.25
;a2	= afb2*.25

asum	balance	sum(ain1, ain2), fractalnoise(.5, .5)

kflw	lineto	k(follow2(asum, .015, .05)), .15
kiflw	= 1-kflw

aflw	a	lineto(kflw*.005, .05)

a1	flanger a1, aflw, limit(kiflw, .5, .995)
a2	flanger a2, aflw, limit(kiflw, .5, .995)

; important, or signal bias grows rapidly
a1 	=	dcblock2(a1) 
a2 	=	dcblock2(a2) 

a1 	=	tanh(a1) 
a2 	=	tanh(a2) 

a1, a2		reverbsc	a1, a2, limit(kflw, .35, .995), 15$k


;	POST
kratio		=	kiflw*randomi:k(2.25, 2.35, .25)	

ideltime 	=	idel/2

ifftsize 	=	512 
ioverlap 	=	ifftsize / 4 
iwinsize 	=	ifftsize 
iwinshape 	=	1; von-Hann window 

fftin		pvsanal	a1, ifftsize, ioverlap, iwinsize, iwinshape 
fftscale 	pvscale	fftin, kratio, 0, 1 
atransL 	pvsynth	fftscale 

fftin2		pvsanal	a2, ifftsize, ioverlap, iwinsize, iwinshape 
fftscale2 	pvscale	fftin2, kratio, 0, 1 
atransR 	pvsynth	fftscale2 

;; delay the feedback to let it build up over time
afb1 	=	vdelay3(atransL, ideltime, ideltime) 
afb2 	=	vdelay3(atransR, ideltime, ideltime) 

;	out
	chnmix a1, "mouth-1"
	chnmix a2, "mouth-2"

;	clearchns
	chnclear S1
	chnclear S2

	endin

;	alwayson
	start("shy")


