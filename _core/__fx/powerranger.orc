	opcode powerranger, 0, SVP
Sin, kshape, kgain xin

;	INs
Sin1	sprintf	"%s-1", Sin
Sin2	sprintf	"%s-2", Sin

ain1	chnget Sin1
ain2	chnget Sin2

kshape	portk	kshape, 5$ms

a1	powershape ain1, kshape + lfo:k(.0015, randomi:k(.005, .05, .05))
a2	powershape ain2, kshape + lfo:k(.0015, randomi:k(.005, .05, .05))

a1	balance	a1, ain1
a2	balance	a2, ain2

a1	*= portk(kgain, 5$ms)
a2	*= portk(kgain, 5$ms)

	chnmix a1, "mouth-1"
	chnmix a2, "mouth-2"

	endop
