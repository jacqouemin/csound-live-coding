	opcode	comber, 0, SkOP
Sin, krvt, klpt, kgain xin

;	INIT
if	klpt <= 0 then
	klpt = giexpzero*10 
endif

Sin1	sprintf	"%s-1", Sin
Sin2	sprintf	"%s-2", Sin

ain1	chnget Sin1
ain2	chnget Sin2

;	instrument
a1	vcomb ain1, krvt, klpt, 15
a2	vcomb ain2, krvt+(krvt/5), klpt, 15

a1	*= kgain
a2	*= kgain

	chnmix a1, "mouth-1"
	chnmix a2, "mouth-2"

	endop
