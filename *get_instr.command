#!/bin/bash

FILE="$(dirname "$0")"/list_instr.md
DIR="$(dirname "$0")"

echo -e "--- INSTRs ---\n\n\n" > "$FILE" # -e enables \n interpretation

find "$DIR"/_core/__instr -type f -name "*.orc" -exec basename {} \; | cut -f 1 -d '.' | sort >> "$FILE"
