# Changelog
Sposored by jacques

## For my future
- add everytime(x, y) equivalent to  if (eu(1, x, y) == 1) for samples
- check difference b/ween floor() and int(), YES IT CHANGE ! (shit!)
- do an opcode that takes euclidean rhythm, but it always five all the pulses of the euclidean, onsets are just accent for dynamic. What about calling it something like "mountains"
- please, please put some order to the LFO family ! (CHECK - if loo is absolute, freq*2) - phase, logarithmic 
- twinkle() & bribes() needs to be in time, not random

# [I'm free version]

## [0.2.0] - 2020-08-11

- I'm back and I started again to think - first all the scales and other things will be outside main files
- subsc --> subscale 

# [Quarantine days version]

## [0.1.0] - 2020-04-30
- added gkbeatn & gkblown (number of completed cycle from the beginning

## [0.1.0] - 2020-04-21
+ [changed in csound plugin vim] now it is possible to write "#" instead of "gk" (e.g. #dorian)

## [0.1.0] - 2020-04-20
+ melodies!

## [0.0.9] - 2020-04-09
### Added
- elle()
- sonre(), reson filter
+ in step if value is 0 = random number

## [0.0.9] - 2020-04-06
### Added
- goi(), invert start/end for go()
- lfo family start growing up, (p.s. square form for absolute lfo is soft because of clicks)
### Changed
- MACROs for dynamics are changed

## [0.0.9] - 2020-04-04
### Added
- starting the new LFOs family, with different table (8192 samples - because of the nature)

## [0.0.8] - 2020-04-02
+ all fx with a schedule are now in the same instrument

## [0.0.7] - 2020-04-01
### Added
- instrument snug()
- witches.detune

## [0.0.6] - 2020-03-31
### Added
- huge changements, WE CAN VOMIT A SCORE NOW!

## [0.0.5] - 2020-03-30
### Added
- gkgain, a master gain
- a little instrument that controls how many instruments are created; if more then 45, it prints

## [0.0.4] - 2020-03-28
### Added
- witches(), synth FM
- go() ramp with cosseg()
+ curve of comeforme(): cosseg()

## [0.0.3] - 2020-03-22
### Added
- followdrum()
- pumps() and breathes() for Strings
- added balance() to puck and repuck

## [0.0.2] - 2020-03-21
### Added
- lfse(), lfo with starting and ending amp
- limit to e() for kdur
- $c & $d macros, respectively for *100 and *10
- balance() in "moog" & in "k35" filters in order to get always the same amp
- lfp(x, y, z), lfo() with a starting point, x = starting point, y = variation, z = freq
- lfpa(x, y, z), lfo() only-positive with a starting point, x = starting point, y = variation, z = freq
+ givemednb(), now receive also dynamics

## [0.0.1] - 2020-03-20
### Added
- fadeaway(x), the opposite of comeforme()
- comeforme(x) a linear ramp from 0 to 1, x = seconds
- step() is similar scall(), better thinking (maybe)
- givemednb(), test to destroy your house
- lfa(), an absolute lfo
- lfi(), an only-integer lfo
- lfia(), an only-integer absolute lfo
+ in e() opcode: amp depends on how many kcps are evoked
+ p4 now is amplitude
+ p5 now is frequency (so be careful to explosion..)

### Removed
- opcode e(), bi(), tri() because of the p changement

### Deprecated
- scall()

## [Defalut]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security

Added - for new features.

Changed - for changes in existing functionality.

Deprecated - for soon-to-be removed features.

Removed - for now removed features.

Fixed - for any bug fixes.

Security - in case of vulnerabilities.
