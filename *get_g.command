#!/bin/bash

temp="$(dirname "$0")"/temp.txt
temp-s="$(dirname "$0")"/temp-s.txt

FILE="$(dirname "$0")"/list_g.md
DIR="$(dirname "$0")"/_core

echo -e "--- Global Ks ---\n\n\n" > "$FILE" # -e enables \n interpretation
echo -e "Gs | notes\r" >> "$FILE"
echo -e "--- | ---\r" >> "$FILE"

find "$DIR" -type f \( -iname "*.orc" ! -iname "scales.orc" \) -exec grep -o -E 'gi\w*' >> "$temp" {} \;
find "$DIR" -type f \( -iname "*.orc" ! -iname "scales.orc" \) -exec grep -o -E 'gk\w*' >> "$temp" {} \;
find "$DIR" -type f \( -iname "*.orc" ! -iname "scales.orc" \) -exec grep -o -E 'gS\w*' >> "$temp" {} \;


sort "$temp" | sed -e 's/$/ | ---/' | uniq > "$temp-s"

# | awk ' {print;} NR % 1 == 0 { print ""; }' --- add newline

cat "$temp-s" >> "$FILE"

rm "$temp"

rm "$temp-s"

sleep 3.5s